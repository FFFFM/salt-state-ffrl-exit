include:
  - ferm

mosh:
  pkg.installed

/etc/ferm/conf.d/40-mosh.conf:
  file.managed:
    - source: salt://mosh/files/ferm.conf
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - file: /etc/ferm/conf.d
