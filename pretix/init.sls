{% import 'restic/init.sls' as restic %}

{% set docker = salt['pillar.get']('docker', {}) %}
{% set pretix = salt['pillar.get']('pretix', {}) %}
{% set volume = docker.volume_root ~ '/pretix' %}
{% set image = 'docker.chaos.expert/ffffm/container/pretix:latest' %}
{% set postgres_image = 'postgres:17.2' %}

include:
  - docker
  - letsencrypt
  - nginx


docker-volume-pretix:
  file.directory:
    - name: {{ volume }}
    - makedirs: True
    - require:
      - file: docker-volume-root

{{ volume }}/config:
  file.directory:
    - user: 15371
    - group: 15371
    - mode: '0755'
    - require:
      - file: docker-volume-pretix


{{ volume }}/config/pretix.cfg:
  file.managed:
    - user: 15371
    - group: 15371
    - mode: '0700'
    - template: jinja
    - source: salt://pretix/files/pretix.cfg.j2
    - require:
      - file: {{ volume }}/config

{{ volume }}/data:
  file.directory:
    - user: 15371
    - group: 15371
    - mode: '0755'
    - require:
      - file: docker-volume-pretix

{{ volume }}/postgres-data:
  file.directory:
    - require:
      - file: docker-volume-pretix

{{ volume }}/redis-data:
  file.directory:
    - require:
      - file: docker-volume-pretix

{{ restic.cmd('docker pretix', 'docker exec -t pretix-postgres sh -c \'PGPASSWORD="$POSTGRES_PASSWORD" pg_dump -U "$POSTGRES_USER" -d "$POSTGRES_DB" > /var/lib/postgresql/data/dump.sql\'') }}
{{ restic.path('docker pretix', volume) }}

docker-image-{{ image }}:
  docker_image.present:
    - name: {{ image }}
    - force: true
    - require:
      - pkg: docker
      - file: docker-volume-root

docker-image-pretix-postgres:
  docker_image.present:
    - name: {{ postgres_image }}
    - force: true
    - require:
      - pkg: docker
      - file: docker-volume-root

docker-image-pretix-redis:
  docker_image.present:
    - name: redis:latest
    - force: true
    - require:
      - pkg: docker
      - file: docker-volume-root


docker-container-pretix-postgres:
  docker_container.running:
    - image: {{ postgres_image }}
    - name: pretix-postgres
    - restart_policy: always
    - environment:
      - POSTGRES_PASSWORD: {{ pretix.get('postgres-password') }}
    - binds:
      - {{ volume }}/postgres-data:/var/lib/postgresql/data
    - require: &postgres-require
      - docker_image: docker-image-pretix-postgres
      - file: {{ volume }}/postgres-data
    - watch: *postgres-require

docker-container-pretix-redis:
  docker_container.running:
    - image: redis
    - name: pretix-redis
    - command: redis-server --appendonly yes
    - restart_policy: always
    - binds:
      - {{ volume }}/redis-data:/data
    - require: &redis-require
      - docker_image: docker-image-pretix-redis
      - file: {{ volume }}/redis-data
    - watch: *redis-require

docker-container-pretix:
  docker_container.running:
    - image: {{ image }}
    - name: pretix
    - command: all
    - restart_policy: always
    - port_bindings:
      - "127.0.0.1:8345:80"
    - links:
      - pretix-postgres: postgres
      - pretix-redis: redis
    - binds:
      - {{ volume }}/config:/etc/pretix
      - {{ volume }}/data:/data
    - require:
      - docker_image: docker-image-{{ image }}
      - docker_container: pretix-postgres
      - docker_container: pretix-redis
      - file: {{ volume }}/config/pretix.cfg
      - file: {{ volume }}/data
    - watch:
      - docker_image: docker-image-{{ image }}
      - file: {{ volume }}/config/pretix.cfg
      - file: {{ volume }}/data


/usr/bin/docker exec pretix pretix cron:
  cron.present:
    - identifier: pretix run periodic
    - minute: 0,15,30,45


prometheus_pretix_export:
  grains.present:
    - value: {{ salt['pillar.get']('pretix:domain', grains['fqdn']) }}:443
