{% if grains['osfinger'] == "Debian-10" %}
{%- if 'dovecot' in salt['pillar.get']('roles', []) %}
include:
  - .dovecot
{%- endif %}
{%- endif %}
