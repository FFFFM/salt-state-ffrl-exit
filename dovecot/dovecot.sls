{% if grains['osfinger'] == "Debian-10" %}
dovecot-repo:
  pkgrepo.managed:
    - humanname: dovecot
    - name: deb https://repo.dovecot.org/ce-2.3-latest/debian/buster buster main
    - key_url: https://repo.dovecot.org/DOVECOT-REPO-GPG
    - file: /etc/apt/sources.list.d/dovecot.list
    - clean_file: true
    - require_in:
      - pkg: dovecot-core
      - pkg: dovecot-packages
{% endif %}

dovecot-core:
  pkg.installed:
    - watch_in:
      - service: dovecot.service
    - require_in:
      - service: dovecot.service

dovecot-packages:
  pkg.installed:
    - pkgs:
      - dovecot-imapd
      - dovecot-ldap
      - dovecot-managesieved
      - dovecot-sieve
    - watch_in:
      - service: dovecot.service
    - require_in:
      - service: dovecot.service

dovecot.service:
  service.running:
    - enable: true
    - reload: True
