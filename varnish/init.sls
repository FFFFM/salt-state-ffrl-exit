{%- set varnish_version = '7.3' %}
include:
  - apt.transport.https
  - apt.repository.sid

varnish:
  pkgrepo.managed:
    - name: "deb https://packagecloud.io/varnishcache/varnish73/debian {{ grains['oscodename'] }} main"
    - key_url: https://packagecloud.io/varnishcache/varnish73/gpgkey
    - file: /etc/apt/sources.list.d/varnish.list
    - clean_file: true
  pkg.latest:
    - pkgs:
      - varnish: {{ varnish_version }}.*
      - varnish-dev: {{ varnish_version }}.*
      - python3-docutils # For building modules
    - require:
        - pkgrepo: varnish
    - refresh: True
  service.running:
    - enable: True
    - require:
        - pkg: varnish

/etc/systemd/system/varnish.service.d/start.conf:
  file.managed:
    - source: salt://varnish/files/start.conf
    - makedirs: True
    - watch_in:
      - service: varnish

prometheus-varnish-exporter-apt-pin:
  file.accumulated:
    - name: apt.sid.pinning_exceptions
    - filename: /etc/apt/preferences.d/sid-pinning
    - text: prometheus-varnish-exporter*
    - require_in:
      - file: /etc/apt/preferences.d/sid-pinning

prometheus-varnish-exporter:
  pkg.installed:
    - pkgs:
      - prometheus-varnish-exporter
    - fromrepo: sid
    - require:
      - pkgrepo: sid

prometheus-addgroup-varnish:
  group.present:
    - name: varnish
    - system: True
    - addusers:
      - prometheus
      - www-data

/etc/ferm/conf.d/40-varnish-exporter.conf:
  file.managed:
    - source: salt://varnish/files/ferm-varnish-exporter.conf
    - user: root
    - group: root
    - mode: '0644'

prometheus_varnish_export:
  grains.present:
    - value: {{ grains.nodename }}:9131

varnish-modules:
  git.latest:
    - name: https://github.com/varnish/varnish-modules.git
    - target: /usr/src/varnish-modules
    - force_fetch: True
    - force_reset: True
    - refspec_branch: {{ varnish_version }}
    - branch: {{ varnish_version }}
    - rev: {{ varnish_version }}
  cmd.run:
    - name: make clean && ./bootstrap && ./configure && make && make check -j 4 && make install && systemctl restart varnish
    - cwd: /usr/src/varnish-modules
    - require:
      - pkg: varnish
      - git: varnish-modules
    - require_in:
        - service: varnish
    - onchanges:
      - git: varnish-modules
