{% if salt['fixes.osmajorrelease']() >= 10 %}
linux-headers-amd64:
  pkg.latest

bpfcc-tools:
  pkg.installed
{% endif %}
