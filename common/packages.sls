ca_certificates:
  pkg.latest:
    - pkgs:
      - ca-certificates

packages_base:
  pkg.installed:
    - pkgs:
      - apt-listbugs
      - curl
      - debian-goodies
      - ethtool
      - file
      - htop
      - iftop
      - ioping
      - iotop
      - iperf3
      - iputils-tracepath
      - jq
      - knot-dnsutils
      - lnav
      - man-db
      - mlocate
      - mtr-tiny
      - ncdu
      - ncurses-term
      - needrestart
      - netcat-openbsd
      - psmisc
      - rsync
      - strace
      - sysstat
      - tcpdump
      - tig
      - tmux
      - traceroute
      - tree
      - vim-nox
      - wget
      - whois
      - zsh

packages_purge:
  pkg.purged:
    - pkgs:
      - inetd
      - puppet
      - puppet-common
      - rsh-server
      - silversearcher-ag
      - telnet-server
      - tftp-server
      - xinetd
      - ypserv
      - rpcbind

purge-exim4:
  pkg.purged:
    - pkgs:
      - exim4
      - bsd-mailx
      - exim4-base
      - exim4-config
      - exim4-daemon-light

include:
  - common.ripgrep
  - common.haveged
  - common.git
  - common.physical
  - common.python
