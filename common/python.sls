python3-systemd:
  pkg.installed

{% if salt['fixes.osmajorrelease']() >= 10 %}
python-pip:
  pkg.installed:
    - pkgs:
      - python3-pip
      - python3-dev
      - python3-virtualenv

python3-virtualenv:
  pkg.installed:
    - pkgs:
      - python3-virtualenv
      - virtualenv

python2-pip:
  pkg.purged:
    - pkgs:
      - python-pip
      - python-dev

{% else %}
python2-pip:
  pkg.installed:
    - pkgs:
      - python-pip
      - python-dev
{% endif %}
