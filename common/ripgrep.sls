{%- if grains['osfinger'] == "Debian-9" %}
include:
  - apt.repository.sid

ripgrep-apt-pin:
  file.accumulated:
    - name: apt.sid.pinning_exceptions
    - filename: /etc/apt/preferences.d/sid-pinning
    - text: ripgrep
    - require_in:
      - file: /etc/apt/preferences.d/sid-pinning
{%- endif %}

ripgrep:
  pkg.installed{%- if grains['osfinger'] == "Debian-9" %}:
    - fromrepo: sid
    - require:
      - pkgrepo: sid
{%- endif %}
