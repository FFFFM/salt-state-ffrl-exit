{% if grains['virtual'] == 'physical' %}
firmware-linux:
  pkg.installed:
    - pkgs:
      - firmware-linux-free
      - firmware-linux-nonfree

smartmontools:
  pkg.installed

numactl:
  pkg.installed

{% else %}
smartmontools:
  pkg.purged: []
{% endif %}
