/etc/vim/vimrc:
  file.managed:
    - source: salt://common/files/vimrc
    - mode: '0644'
    - user: root
    - group: root
