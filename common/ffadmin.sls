include:
  - ssh
  - common.sudo

ffadmin:
  user.present:
    - shell: /bin/bash
    - groups:
      - sudo
      - ssh-user
    - require:
      - group: ssh-user

/home/ffadmin/.ssh/authorized_keys:
  file.managed:
    - source: salt://common/files/authorized_keys.tpl
    - user: ffadmin
    - group: ffadmin
    - mode: '0600'
    - makedirs: True
    - template: jinja
    - require:
      - user: ffadmin

/home/ffadmin/.screenrc:
  file.managed:
    - source: salt://common/files/screenrc.root
