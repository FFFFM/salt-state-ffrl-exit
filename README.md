[![build status](https://chaos.expert/FFFFM/salt-state-ffrl-exit/badges/master/build.svg)](https://chaos.expert/FFFFM/salt-state-ffrl-exit/commits/master)

FFFFM [Salt states](https://docs.saltstack.com/en/latest/)


## Is it any good?

[Yes](https://news.ycombinator.com/item?id=3067434)

## Is it awesome?

[These people](https://twitter.com/FreiFunkFFM/followers) and [those](https://twitter.com/ffffm_noc/followers) seem to like it.
