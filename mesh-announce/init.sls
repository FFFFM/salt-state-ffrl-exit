{% set mesh_announce_git = 'https://github.com/ffnord/mesh-announce.git' %}

include:
  - ferm

{{ mesh_announce_git }}:
  git.latest:
    - target: /opt/mesh-announce
    - force_fetch: true
    - force_reset: true

/etc/systemd/system/mesh-announce@.service:
  file.absent

/etc/systemd/system/respondd.service:
  file.managed:
    - source: salt://mesh-announce/files/respondd.service
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - {{ mesh_announce_git }}
    - watch:
      - {{ mesh_announce_git }}

mesh-announce_deps:
  pkg.installed:
    - pkgs:
      - ethtool
      - lsb-release
      - python3-netifaces

/etc/mesh-announce:
  file.directory

/etc/mesh-announce/respondd.conf:
  file.managed:
    - source: salt://mesh-announce/files/respondd.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - require:
      - /etc/mesh-announce

respondd:
  service.running:
    - enable: true
    - require:
      - /etc/systemd/system/respondd.service
      - /etc/mesh-announce/respondd.conf
    - watch:
      - /etc/systemd/system/respondd.service
      - /etc/mesh-announce/respondd.conf

/etc/ferm/conf.d/50-mesh-announce.conf:
  file.managed:
    - source: salt://mesh-announce/files/ferm.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - pkg: ferm
