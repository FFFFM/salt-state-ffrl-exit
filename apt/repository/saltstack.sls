include:
  - apt.transport.https
  - apt.dependencies

saltstack-repo:
  pkgrepo.managed:
    - humanname: saltstack
    - name: "deb [signed-by=/etc/apt/keyrings/salt-archive-keyring.pgp arch=amd64] https://packages.broadcom.com/artifactory/saltproject-deb/ stable main"
    - file: /etc/apt/sources.list.d/saltstack.list
    - clean_file: True
    - aptkey: False
    - key_url: https://packages.broadcom.com/artifactory/api/security/keypair/SaltProjectKey/public
