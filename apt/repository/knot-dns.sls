include:
  - apt.transport.https
  - apt.dependencies

knot-dns-deb:
  pkgrepo.managed:
    - humanname: knot-dns.cz/knot/
{% if grains['osmajorrelease'] == 12 %}
    - name: deb https://deb.knot-dns.cz/knot-latest bookworm main
    - dist: bookworm
{% elif grains['osmajorrelease'] == 11 %}
    - name: deb https://deb.knot-dns.cz/knot-latest bullseye main
    - dist: bullseye
{% elif grains['osmajorrelease'] == 10 %}
    - name: deb https://deb.knot-dns.cz/knot-latest buster main
    - dist: buster
{%- elif grains['osfinger'] == 'Debian-9' %}
    - name: deb https://deb.knot-dns.cz/knot-latest stretch main
    - dist: stretch
{%- endif %}
    - file: /etc/apt/sources.list.d/knot.list
    - clean_file: True
    - key_url: https://deb.knot-dns.cz/apt.gpg

knot-common:
  pkg.installed:
    - pkgs:
      - knot-dnsutils
      - knot-host
