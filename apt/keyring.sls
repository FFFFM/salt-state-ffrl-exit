---
/etc/apt/keyrings:
  file.directory:
    - user: root
    - group: root
    - dir_mode: "0755"
