include:
  - apt.transport.https
  - apt.keyring
  - apt.unattended-upgrades
  - apt.dependencies

{% set oscodename = grains['oscodename'] %}
{% set osmajorrelease = grains['osmajorrelease'] %}

{% if grains['osfinger'] == "Debian-9" %}
/etc/apt/sources.list:
  file.managed:
    - contents: |
        deb https://archive.debian.org/debian stretch main contrib non-free
        deb-src https://archive.debian.org/debian stretch main

        deb https://archive.debian.org/debian-security stretch/updates main contrib non-free
        deb-src https://archive.debian.org/debian-security stretch/updates main

        deb https://archive.debian.org/debian stretch-proposed-updates main contrib non-free
        deb-src https://archive.debian.org/debian stretch-proposed-updates main

stretch-backports:
  pkgrepo.managed:
    - name: deb https://archive.debian.org/debian stretch-backports main contrib non-free
    - file: /etc/apt/sources.list.d/backports.list
    - clean_file: True
{% else %}
/etc/apt/sources.list:
  file.managed:
    - contents: |
        deb https://deb.debian.org/debian {{ oscodename }} main contrib non-free{% if osmajorrelease >= 12 %} non-free-firmware{%- endif %}

        deb https://deb.debian.org/debian {{ oscodename }}-updates main contrib non-free{% if osmajorrelease >= 12 %} non-free-firmware{%- endif %}

        deb https://deb.debian.org/debian {{ oscodename }}-proposed-updates main contrib non-free{% if osmajorrelease >= 12 %} non-free-firmware{%- endif %}

        deb https://deb.debian.org/debian {{ oscodename }}-backports main contrib non-free{% if osmajorrelease >= 12 %} non-free-firmware{%- endif %}
{% if osmajorrelease > 10 %}
        deb https://deb.debian.org/debian-security {{ oscodename }}-security main contrib non-free{% if osmajorrelease >= 12 %} non-free-firmware{%- endif %}
{% else %}
        deb https://security.debian.org/debian-security {{ oscodename }}/updates main contrib non-free
{%- endif %}

/etc/apt/sources.list.d/backports.list:
  file.absent
{% endif %}
