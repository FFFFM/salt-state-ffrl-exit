{% if salt['fixes.osmajorrelease']() < 10 %}
apt-transport-https:
  pkg.installed:
    - order: 1
{% endif %}
