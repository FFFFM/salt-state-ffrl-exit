include:
  - apt.repository.saltstack

salt-minion:
  pkg.latest:
{%- if grains['osfinger'] == 'Debian-9' %}
    - version: '3004+*'
{%- else %}
    - version: '3006+*'
{%- endif %}
  service.running:
    - enable: True
    - watch:
      - file: /etc/salt/minion.d

/etc/salt/minion.d:
  file.recurse:
    - source: salt://salt/files/minion.d
    - clean: True
    - exclude_pat: _schedule.conf

# deploy killmode workaround to fix broken minion upgrades
# https://github.com/saltstack/salt/issues/7997#issuecomment-160913751
/etc/systemd/system/salt-minion.service.d/killmode.conf:
  file.managed:
    - source: salt://salt/files/killmode.conf
    - makedirs: True
