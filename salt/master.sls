{% import 'restic/init.sls' as restic %}

include:
  - apt.repository.saltstack

salt-master:
  pkg.latest:
    - version: '3006+*'
  service.running:
    - enable: True
    - watch:
      - file: /etc/salt/master.d

gitpython:
  pip.installed:
    - name: gitpython

/etc/salt/master.d:
  file.recurse:
    - source: salt://salt/files/master.d

/etc/ferm/conf.d/40-salt-zmq.conf:
  file.managed:
    - source: salt://salt/files/ferm.conf
    - user: root
    - group: root
    - mode: '0644'

/opt/saltstack/salt/.ssh:
  file.directory:
    - user: salt
    - group: salt
    - mode: '0700'

{{ restic.path('salt master', '/etc/salt/pki/', '/var/cache/salt/master/', '/opt/saltstack/salt/.ssh/') }}
