{% import 'restic/init.sls' as restic %}

include:
  - ferm
  - apt.transport.https
  - apt.dependencies

gitlab-ee:
  pkgrepo.managed:
    - name: "deb https://packages.gitlab.com/gitlab/gitlab-ee/debian {{ grains['oscodename'] }} main"
    - key_url: https://packages.gitlab.com/gitlab/gitlab-ee/gpgkey
    - file: /etc/apt/sources.list.d/gitlab.list
  pkg.installed:
    - require:
      - pkgrepo: gitlab-ee

{{ restic.cmd('gitlab', 'gitlab-backup create SKIP=artifacts,registry,tar') }}
{{ restic.path('gitlab', '/var/opt/gitlab/backups/') }}
{{ restic.exclude('gitlab', '/var/opt/gitlab/backups/tmp/') }}

/etc/gitlab/gitlab.rb:
  file.managed:
    - source: salt://gitlab/files/gitlab.rb.j2
    - template: jinja
    - user: root
    - group: root
    - mode: '0600'
    - require:
      - pkg: gitlab-ee

gitlab-ctl reconfigure:
  cmd.run:
    - onchanges:
      - file: /etc/gitlab/gitlab.rb

gitlab-ctl restart:
  cmd.run:
    - onchanges:
      - cmd: gitlab-ctl reconfigure

/var/www/gitlab-robots.txt:
  file.managed:
    - source: salt://gitlab/files/robots.txt
    - makedirs: True
    - user: www-data
    - group: www-data
    - mode: '0644'

/etc/ferm/conf.d/40-gitlab.conf:
  file.managed:
    - source: salt://gitlab/files/ferm.conf
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - pkg: ferm
