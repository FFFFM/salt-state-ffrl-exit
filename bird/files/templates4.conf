################################################################################
#				BGP templates				 #
################################################################################

template bgp general_bgp {
	local as PUBLIC_AS;
	add paths on;
	enable route refresh on;
	graceful restart time 300;
	med metric on;
	import keep filtered;
}

template bgp general_ebgp {
	local as PUBLIC_AS;
	add paths on;
	enable route refresh on;
	graceful restart time 300;
	med metric on;
	next hop self;
	import keep filtered;
	bfd on;
}


##########################################
#       Upstream BGP templates	   #
##########################################

template bgp global_transit from general_ebgp {
	import filter global_transit_in;
	export filter global_transit_out;
}

template bgp metro_transit from general_ebgp {
	import filter metro_transit_in;
	export filter metro_transit_out;
}

template bgp cix_transit from general_ebgp {
	import filter cix_transit_in;
	export filter cix_transit_out;
}

##########################################
#	Peering BGP templates	   #
##########################################

template bgp global_peer from general_ebgp {
	import filter global_peer_in;
	export filter global_peer_out;
}

template bgp metro_peer from general_ebgp {
	import filter metro_peer_in;
	export filter metro_peer_out;
}

template bgp px_global_peer from general_ebgp {
	import filter px_global_peer_in;
	export filter global_peer_out;
}

template bgp px_metro_peer from general_ebgp {
	import filter px_metro_peer_in;
	export filter metro_peer_out;
}

template bgp px_global_backup_peer from general_ebgp {
	import filter px_global_backup_peer_in;
	export filter global_peer_out;
}

template bgp px_metro_backup_peer from general_ebgp {
	import filter px_metro_backup_peer_in;
	export filter metro_peer_out;
}

template bgp px_t1_peer from general_ebgp {
	import filter px_t1_peer_in;
	export filter metro_peer_out;
}

template bgp route_server from general_ebgp {
	import filter route_server_in;
	export filter metro_peer_out;
}

##########################################
#       Downstream BGP templates	 #
##########################################

template bgp ebgp_community from general_ebgp {
	export filter community_out;
}

template bgp bilateral_metro_transit from general_ebgp {
	import filter drain;
	export filter bilateral_metro_transit_out;
}

template bgp metro_transit_customer from general_ebgp {
	import filter drain;
	export filter metro_transit_customer_out;
}

template bgp bilateral_global_transit from general_ebgp {
	import filter drain;
	export filter bilateral_global_transit_out;
}

template bgp global_transit_customer from general_ebgp {
	import filter drain;
	export filter global_transit_customer_out;
}

template bgp lg from general_ebgp {
	import filter drain;
	export filter global_transit_customer_out;
	add paths off;
	multihop 10;
	bfd off;
}

template bgp lg_ring_nlnog from lg {
	multihop 15;
}

template bgp lg_dfzwatch from lg {
	multihop 10;
}

template bgp rrc from lg {
	import filter as12654_RIS_in;
	direct;
}

template bgp lg_bgp_tools from lg {
	multihop 255;
}
