include:
  - ferm
  - bird.exporter
{%- if 'gateway' in salt['pillar.get']('roles', [])  %}
  - bird.domains
{%- endif %}

bird:
  pkg.installed:
    - pkgs:
      - bird
  service.running:
    - enable: True
    - reload: True
    - watch:
      - file: /etc/bird/bird.conf
      - file: /etc/bird/bird.d/*

bird6:
  service.running:
    - enable: True
    - reload: True
    - watch:
      - file: /etc/bird/bird6.conf
      - file: /etc/bird/bird6.d/*

/etc/ferm/conf.d/40-bird.conf:
  file.managed:
    - source: salt://bird/files/ferm.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - file: /etc/ferm/conf.d


/etc/bird/bird.conf:
  file.managed:
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d
    - contents: |
        include "/etc/bird/bird.d/*.conf";

/etc/bird/bird6.conf:
  file.managed:
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d
    - contents: |
        include "/etc/bird/bird6.d/*.conf";

/etc/bird/bird.d:
  file.directory:
    - user: root
    - group: root
    - require:
      - pkg: bird

/etc/bird/bird6.d:
  file.directory:
    - user: root
    - group: root
    - require:
      - pkg: bird

/etc/bird/bird.d/10-local.conf:
  file.managed:
    - source: salt://bird/files/local.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: '0644'
    - context:
        version: 'v4'
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d

/etc/bird/bird6.d/10-local.conf:
  file.managed:
    - source: salt://bird/files/local.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: '0644'
    - context:
        version: 'v6'
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d


/etc/bird/bird.d/12-announce.conf:
  file.managed:
    - source: salt://bird/files/announce.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: '0644'
    - context:
        type: 'bgp4'
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d

/etc/bird/bird6.d/12-announce.conf:
  file.managed:
    - source: salt://bird/files/announce.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: '0644'
    - context:
        type: 'bgp6'
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d

/etc/bird/bird.d/900-kernel.conf:
  file.managed:
    - source: salt://bird/files/kernel.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: '0644'
    - context:
        ip_version: 4
        kernel_table: main
        pipe_export_filter: {{ salt['pillar.get']('pipe_main_export_filter', "drain") }}
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d

/etc/bird/bird6.d/900-kernel.conf:
  file.managed:
    - source: salt://bird/files/kernel.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: '0644'
    - context:
        ip_version: 6
        kernel_table: main
        pipe_export_filter: {{ salt['pillar.get']('pipe_main_export_filter', "drain") }}
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d
