include:
  - bird
{% if pillar.get('docker', {}).get('ipv6') %}
  - bird.docker
{%- endif %}

/etc/bird/bird.d/20-common-policy.conf:
  file.managed:
    - source: salt://bird/files/common-policy.conf
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d

/etc/bird/bird6.d/20-common-policy.conf:
  file.managed:
    - source: salt://bird/files/common-policy.conf
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d


/etc/bird/bird.d/21-routing-policy.conf:
  file.managed:
    - source: salt://bird/files/routing-policy4.conf
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d

/etc/bird/bird6.d/21-routing-policy6.conf:
  file.managed:
    - source: salt://bird/files/routing-policy6.conf
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d


/etc/bird/bird.d/22-templates.conf:
  file.managed:
    - source: salt://bird/files/templates4.conf
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d

/etc/bird/bird6.d/22-templates.conf:
  file.managed:
    - source: salt://bird/files/templates6.conf
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d

/etc/bird/bird.d/23-ibgp.conf:
  file.managed:
    - source: salt://bird/files/bgp/ibgp.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - context:
        ip_version: 4
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d

/etc/bird/bird6.d/23-ibgp.conf:
  file.managed:
    - source: salt://bird/files/bgp/ibgp.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - context:
        ip_version: 6
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d

/etc/bird/bird.d/20-google-more-specific.conf:
  file.managed:
    - source: salt://bird/files/bgp/google_more_specific4.conf
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d

/etc/bird/bird.d/23-bgp-route-reflector.conf:
  file.absent

/etc/bird/bird6.d/23-bgp-route-reflector.conf:
  file.absent

/etc/bird/bird.d/24-route-reflector.conf:
  file.managed:
    - source: salt://bird/files/bgp/route-reflector.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - context:
        ip_version: 4
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d

/etc/bird/bird6.d/24-route-reflector.conf:
  file.managed:
    - source: salt://bird/files/bgp/route-reflector.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - context:
        ip_version: 6
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d

/etc/bird/bird.d/25-de-cix-common.conf:
  file.managed:
    - source: salt://bird/files/bgp/de-cix-common.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d

/etc/bird/bird6.d/25-de-cix-common.conf:
  file.managed:
    - source: salt://bird/files/bgp/de-cix-common.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d

/etc/bird/bird.d/26-de-cix.conf:
  file.managed:
    - source: salt://bird/files/bgp/de-cix4.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d

/etc/bird/bird6.d/26-de-cix.conf:
  file.managed:
    - source: salt://bird/files/bgp/de-cix6.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d

/etc/bird/bird.d/26-kleyrex.conf:
  file.managed:
    - source: salt://bird/files/bgp/kleyrex.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d

/etc/bird/bird6.d/26-kleyrex.conf:
  file.managed:
    - source: salt://bird/files/bgp/kleyrex.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d

/etc/bird/bird.d/27-lwlcom.conf:
  file.managed:
    - source: salt://bird/files/bgp/lwlcom.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d

/etc/bird/bird6.d/27-lwlcom.conf:
  file.managed:
    - source: salt://bird/files/bgp/lwlcom.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d

/etc/bird/bird.d/30-peers.conf:
  file.managed:
    - source: salt://bird/files/peering.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: '0644'
    - context:
        type: 'bgp4'
    - require:
      - pkg: bird
      - file: /etc/bird/bird.d

/etc/bird/bird6.d/30-peers.conf:
  file.managed:
    - source: salt://bird/files/peering.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: '0644'
    - context:
        type: 'bgp6'
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d
