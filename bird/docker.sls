{% set docker = pillar.get('docker', {}) %}

{% if docker.get('ipv6') %}
/etc/bird/bird6.d/40-docker.conf:
  file.managed:
    - source: salt://bird/files/docker6.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: '0644'
    - context:
        prefix: {{ docker.ipv6 }}
    - require:
      - pkg: bird
      - file: /etc/bird/bird6.d
{%- endif %}
