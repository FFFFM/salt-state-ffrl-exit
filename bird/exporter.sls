{%- set gopath = salt['pillar.get']('golang:gopath', '/usr/local/go') %}
{%- set gopkg = 'github.com/czerwonk/bird_exporter' %}

include:
  - golang
  - common.git
  - ferm

bird-exporter:
  git.latest:
    - name: https://{{ gopkg }}.git
    - target: {{ gopath }}/src/{{ gopkg }}
    - force_fetch: true
    - force_reset: true
    - rev: f24f1ab36c60d77077f0a60686a80e6844fcf2f1
    - refspec_branch: main
    - require:
      - pkg: git
  cmd.run:
    - cwd: {{ gopath }}/src/{{ gopkg }}
    - name: go install -v {{ gopkg }}
    - env:
        GOPATH: {{ gopath }}
        GOCACHE: /tmp/GOCACHE/
        GO111MODULE: "on"
    - require:
      - pkg: golang
      - pkg: git
      - git: bird-exporter
    - onchanges:
      - git: bird-exporter
  service.running:
    - enable: True
    - require:
      - file: /etc/systemd/system/bird-exporter.service
    - watch:
      - file: /etc/systemd/system/bird-exporter.service
      - cmd: bird-exporter

/etc/systemd/system/bird-exporter.service:
  file.managed:
    - source: salt://bird/files/exporter/bird-exporter.service
    - user: root
    - group: root
    - mode: '0644'

/etc/ferm/conf.d/40-bird-exporter.conf:
  file.managed:
    - source: salt://bird/files/exporter/ferm-bird-exporter.conf
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - file: /etc/ferm/conf.d

prometheus_bird_export:
  grains.present:
    - value: {{ grains.nodename }}:9324
