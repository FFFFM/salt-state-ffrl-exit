---
# Disable systemd-timesyncd
#
systemd-timesyncd:
  service.dead:
    - enable: False

# Install and enable chrony instead
#
chrony:
  pkg.installed:
    - pkgs:
      - chrony
{%- if grains['osfinger'] == "Debian-10" %}
    - fromrepo: buster-backports
{% endif %}
  service.running:
    - enable: True
    - name: chrony

/etc/chrony/chrony.conf:
  file.managed:
    - source: salt://ntp/files/chrony_config.j2
    - template: jinja
    - user: root
    - group: root
    - mode: '0644'
    - watch_in:
      - service: chrony

{%- if 'ntp' in salt['pillar.get']('roles', []) %}
/etc/ferm/conf.d/40-chrony.conf:
  file.managed:
    - source: salt://ntp/files/ferm.conf
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - require:
      - file: /etc/ferm/conf.d
{%- endif %}
