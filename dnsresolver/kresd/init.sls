include:
  - ferm
  - apt.repository.knot-dns
  - letsencrypt
  - nginx

knot-resolver:
  pkg.latest:
    - pkgs:
      - knot-resolver
      - dns-root-data
      - knot-dnsutils
      - knot-host
      - knot-resolver-module-http
      - lua-http
      - lua-filesystem
  service.running:
    - name: kresd@1.service
    - enable: True
    - watch:
      - pkg: knot-resolver
      - file: /etc/knot-resolver/kresd.conf

/etc/knot-resolver/kresd.conf:
  file.managed:
    - source: salt://dnsresolver/kresd/files/kresd.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja

/etc/systemd/system/sockets.target.wants/kresd.socket:
  file.absent

/etc/network/interfaces.d/dns-anycast-lo:
  file.managed:
    - source: salt://dnsresolver/files/anycast-lo.conf.j2
    - mode: '0644'
    - user: root
    - group: root
    - template: jinja

/etc/ferm/conf.d/40-kresd.conf:
  file.managed:
    - source:
      - salt://dnsresolver/kresd/files/ferm.conf.j2
      - salt://dnsresolver/files/ferm-default.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - require:
      - file: /etc/ferm/conf.d

prometheus_kresd_export:
  grains.present:
    - value: {{ grains.nodename }}:443
