include:
  - ferm
  - dnsresolver.unbound.exporter

unbound:
  pkg.latest:
    - pkgs:
      - unbound
{%- if grains['osfinger'] == "Debian-11" %}
    - fromrepo: bullseye-backports
{% endif %}
  service.running:
    - enable: True
    - reload: True
    - watch:
      - file: /etc/unbound/unbound.conf

dns-root-data:
  pkg.installed

/etc/unbound/unbound.conf:
  file.managed:
    - source: salt://dnsresolver/unbound/files/unbound.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja

/etc/unbound/unbound_server.pem:
  file.managed:
    - user: root
    - group: unbound

/etc/unbound/unbound_control.pem:
  file.managed:
    - user: unbound
    - group: unbound

/etc/unbound/unbound_control.key:
  file.managed:
    - user: unbound
    - group: unbound

/etc/network/interfaces.d/dns-anycast-lo:
  file.managed:
    - source: salt://dnsresolver/files/anycast-lo.conf.j2
    - mode: '0644'
    - user: root
    - group: root
    - template: jinja

/etc/ferm/conf.d/40-unbound.conf:
  file.managed:
    - source:
      - salt://dnsresolver/unbound/files/ferm.conf.j2
      - salt://dnsresolver/files/ferm-default.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - require:
      - file: /etc/ferm/conf.d
