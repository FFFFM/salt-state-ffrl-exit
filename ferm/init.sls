{%- set roles = salt['pillar.get']('roles', []) -%}

/etc/ferm/conf.d:
  file.directory:
    - user: root
    - group: root

# this fixes issues with file globbing, see https://github.com/saltstack/salt/issues/24436
/etc/ferm/conf.d/.keep:
  file.managed:
    - replace: False

# deprecated: dependency issues on boot made us move to the systemd unit
/etc/default/ferm:
  file.absent

/etc/systemd/system/ferm.service:
  file.managed:
    - source: salt://ferm/files/ferm.service
    - user: root
    - group: root
    - mode: '0644'

ferm:
  pkg.installed:
    - pkgs:
      - ferm
  service.running:
    - enable: True
    - reload: True
    - require:
      - file: /etc/systemd/system/ferm.service
    - watch:
      - file: /etc/ferm/ferm.conf
      - file: /etc/ferm/conf.d/*

include:
{%- if 'vmhost' in roles or 'edge' in roles or 'noconntrack' in roles %}
  - ferm.stateless
{%- else %}
  - ferm.stateful
{%- endif %}
