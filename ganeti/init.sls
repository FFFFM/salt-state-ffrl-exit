include:
  - apt.repository.sid

init-system-helpers:
  pkg.latest:
    - pkgs:
      - init-system-helpers

ganeti:
  pkg.installed:
    - pkgs:
      - ganeti
      - ganeti-instance-debootstrap
      - ganeti-os-noop
{% if grains['osmajorrelease'] == 11 %}
    - fromrepo: bullseye
{% elif grains['osmajorrelease'] == 10 %}
    - fromrepo: buster-backports
{%- endif %}

ganeti_dependencies:
  pkg.installed:
    - pkgs:
      - drbd-utils

ganeti_dependencies_backports:
  pkg.installed:
    - pkgs:
      - qemu-system-x86
{% if grains['osmajorrelease'] == 11 %}
    - fromrepo: bullseye-backports
{% elif grains['osmajorrelease'] == 10 %}
    - fromrepo: buster-backports
{%- endif %}

ganeti_dependencies_sid:
  pkg.installed:
    - fromrepo: sid
    - require:
      - pkgrepo: sid
    - pkgs:
      - ubuntu-keyring

ubuntu-keyring-apt-pin:
  file.accumulated:
    - name: apt.sid.pinning_exceptions
    - filename: /etc/apt/preferences.d/sid-pinning
    - text: ubuntu-keyring
    - require_in:
      - file: /etc/apt/preferences.d/sid-pinning

# drbd replication
/etc/modprobe.d/drbd.conf:
  file.managed:
    - contents: options drbd minor_count=128 usermode_helper=/bin/true

drbd:
  kmod.present:
    - persist: True

# lvm: don't scan drbd devices for lvm volumes
/etc/lvm/lvm.conf:
  file.managed:
    - source: salt://ganeti/files/lvm.conf.{{ grains['oscodename'] }}

# firewalling on the cluster management network
/etc/ferm/conf.d/40-ganeti.conf:
  file.managed:
    - source: salt://ganeti/files/ferm.conf.j2
    - template: jinja
    - user: root
    - group: root
    - mode: '0644'

# instance-debootstrap configuration (hooks & variants)
/etc/ganeti/instance-debootstrap/hooks:
  file.recurse:
    - source: salt://ganeti/files/hooks
    - clean: True
    - file_mode: '0755'
    - dir_mode: '0755'
    - require:
      - pkg: ganeti

/etc/ganeti/instance-debootstrap/variants.list:
  file.managed:
    - source: salt://ganeti/files/variants.list
    - require:
      - pkg: ganeti

/etc/ganeti/instance-debootstrap/variants:
  file.recurse:
    - source: salt://ganeti/files/variants
    - clean: True
    - file_mode: '0644'
    - dir_mode: '0755'
    - require:
      - pkg: ganeti

/etc/ganeti/kvm-vif-bridge:
  file.managed:
    - source: salt://ganeti/files/kvm-vif-bridge
    - user: root
    - group: root
    - mode: '0755'
