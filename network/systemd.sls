#
## Networking via systemd-networkd
#

include:
  - systemd

{% if salt['pillar.get']('is_vm', false) and salt['pillar.get']('cluster_type', '') == 'ganeti' %}
{% set vm_interface = 'ens13' %}
/etc/systemd/network/20-{{ vm_interface }}.network:
  file.managed:
    - source: salt://network/files/systemd/vm_interface.network.j2
    - mode: '0644'
    - user: root
    - group: root
    - template: jinja
    - context:
        name: {{ vm_interface }}
{% endif %}

ifupdown:
  pkg.purged

ifupdown2:
  pkg.purged

/etc/network/ifupdown2:
  file.absent

/etc/network/interfaces:
  file.absent

clean /etc/network/interfaces.d/:
  file.directory:
    - name: /etc/network/interfaces.d
    - user: root
    - group: root
    - mode: '0755'
    - clean: true

systemd-networkd.service:
  service.running:
    - enable: true
    - running: true
    - watch:
      - file: /etc/systemd/network/*
