#
## Networking via ifupdown2
#

ifupdown2:
  pkg.installed: []
  cmd.run:
    - name: /sbin/ifreload -af
    - onchanges:
      - pkg: ifupdown2

# ifupdown2 configuration
/etc/network/ifupdown2/ifupdown2.conf:
  file.managed:
    - source: salt://network/files/ifupdown2.conf
    - require:
      - pkg: ifupdown2

clean /etc/systemd/network/:
  file.directory:
    - name: /etc/systemd/network
    - user: root
    - group: root
    - mode: '0755'
    - clean: true

# Reload interface configuration if neccessary
ifreload:
  cmd.run:
    - name: /sbin/ifreload -af
    - require:
      - file: /etc/network/ifupdown2/ifupdown2.conf
