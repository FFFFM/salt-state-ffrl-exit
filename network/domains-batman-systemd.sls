include:
  - systemd
  - network.systemd

{% set transport_interface = salt['pillar.get']('ferm:transport_interface', 'ens14') %}
/etc/systemd/network/20-{{ transport_interface }}.network:
  file.managed:
    - source: salt://network/files/systemd/transport_interface.network.j2
    - mode: '0644'
    - user: root
    - group: root
    - template: jinja
    - context:
        name: {{ transport_interface }}

/etc/ferm/conf.d/20-{{ transport_interface }}-vxlan.conf:
  file.managed:
    - source: salt://network/files/ferm-vxlan.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - context:
        name: {{ transport_interface }}

/etc/ferm/conf.d/20-vtep-vxlan.conf:
  file.managed:
    - source: salt://network/files/ferm-vxlan-vtep.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - context:
        vtep: {{ pillar['vtep'] }}

/etc/systemd/system/dom@.service:
  file.absent

dom.service:
  service.masked

{% for domain in salt['pillar.get']('domains', {}).keys() %}
{% set domain_id = salt['pillar.get']('domains:%s:domain_id' | format(domain)) %}
/opt/multidomain/dom_{{ domain_id }}_up.sh:
  file.absent

/opt/multidomain/dom_{{ domain_id }}_down.sh:
  file.absent

/etc/network/interfaces.d/dom{{ domain_id }}:
  file.absent

{% if 'gateway' in salt['pillar.get']('roles', []) %}
/etc/ferm/conf.d/20-dom{{ domain_id }}.conf:
  file.managed:
    - source: salt://network/files/ferm-domain-batman.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - context:
        domain: {{ domain }}
        domain_id: {{ domain_id }}
        routing: {{ pillar['routing'] }}
{% endif %}


/etc/systemd/network/30-{{ domain }}-bat.netdev:
  file.managed:
    - source: salt://network/files/systemd/batadv.netdev.j2
    - mode: '0644'
    - user: root
    - group: root
    - template: jinja
    - context:
        domain: {{ domain }}
        domain_id: {{ domain_id }}
/etc/systemd/network/30-{{ domain }}-bat.network:
  file.managed:
    - source: salt://network/files/systemd/batadv.network.j2
    - mode: '0644'
    - user: root
    - group: root
    - template: jinja
    - context:
        domain: {{ domain }}
        domain_id: {{ domain_id }}


/etc/systemd/network/40-{{ domain }}-tp.netdev:
  file.managed:
    - source: salt://network/files/systemd/vxlan_batadv.netdev.j2
    - mode: '0644'
    - user: root
    - group: root
    - template: jinja
    - context:
        domain: {{ domain }}
        domain_id: {{ domain_id }}
/etc/systemd/network/40-{{ domain }}-tp.network:
  file.managed:
    - source: salt://network/files/systemd/vxlan_batadv.network.j2
    - mode: '0644'
    - user: root
    - group: root
    - template: jinja
    - context:
        domain: {{ domain }}
        domain_id: {{ domain_id }}
        vtep: {{ pillar['vtep'] }}


{% if 'gateway' in salt['pillar.get']('roles', []) %}
/etc/systemd/network/50-{{ domain }}-peers-br.netdev:
  file.managed:
    - source: salt://network/files/systemd/peers_bridge.netdev.j2
    - mode: '0644'
    - user: root
    - group: root
    - template: jinja
    - context:
        domain: {{ domain }}
        domain_id: {{ domain_id }}
/etc/systemd/network/50-{{ domain }}-peers-br.network:
  file.managed:
    - source: salt://network/files/systemd/peers_bridge.network.j2
    - mode: '0644'
    - user: root
    - group: root
    - template: jinja
    - context:
        domain: {{ domain }}
        domain_id: {{ domain_id }}


/etc/systemd/network/60-{{ domain }}-vpn.network:
  file.managed:
    - source: salt://network/files/systemd/fastd_peer.network.j2
    - mode: '0644'
    - user: root
    - group: root
    - template: jinja
    - context:
        domain: {{ domain }}
        domain_id: {{ domain_id }}
{% endif %}

dom@{{ domain_id }}.service:
  service.masked
{% endfor %}
