#
## Networking generic
#

{% set network_cfg_daemon = salt['pillar.get']('network_cfg_daemon', "ifupdown2") %}

iproute2:
  pkg.installed

network-pkg:
  pkg.installed:
    - pkgs:
      - bridge-utils
      - vlan
      - tcpdump
      - vnstat
      - knot-host
      - ipv6calc

remove-netplan:
  pkg.purged:
    - pkgs:
      - netplan
      - netplan.io

/etc/netplan:
  file.absent

include:
{%- if network_cfg_daemon == 'ifupdown2' %}
  - network.ifupdown2
{%- elif network_cfg_daemon == 'systemd' %}
  - network.systemd
{%- endif %}

{% if grains['virtual'] == 'physical' %}
{% import 'restic/init.sls' as restic %}
{{ restic.path('network', '/etc/network/') }}
{% endif %}
