# NGINX
## Anonymize IPs
To log real ip addresses use the following pillar

```yaml
nginx:
  logs:
    anonymize_ips: False
```
