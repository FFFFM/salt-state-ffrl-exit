# webroot challenge snippet
/etc/nginx/snippets/acme-challenge.conf:
  file.managed:
    - source: salt://nginx/files/snippets/acme-challenge.conf.tpl
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - pkg: nginx
      - file: /etc/nginx/snippets

/srv/letsencrypt:
  file.directory

# ssl snippets
/etc/nginx/snippets/ssl-redirect.conf:
  file.managed:
    - source: salt://nginx/files/snippets/redirect_to_ssl.conf
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - pkg: nginx
      - file: /etc/nginx/snippets

/etc/nginx/snippets/ssl.conf:
  file.managed:
    - source: salt://nginx/files/snippets/ssl.conf
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - pkg: nginx
      - file: /etc/ssl/ffdhe4096.pem

/etc/systemd/system/certbot.service.d/nginx.conf:
  file.absent
# reload openresty after certbot run
/etc/systemd/system/certbot.service.d/openresty.conf:
  file.managed:
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - file: /etc/systemd/system/certbot.service.d
    - contents: |
        [Service]
        ExecStartPost=/bin/systemctl reload openresty
