/etc/nginx/snippets/metrics.conf:
  file.managed:
    - source: salt://nginx/files/snippets/metrics.conf
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - pkg: nginx

/etc/nginx/conf.d/metrics.conf:
  file.managed:
    - source: salt://nginx/files/conf.d/metrics.conf
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - require:
      - pkg: nginx

prometheus_nginx_export:
  grains.present:
    - require:
      - file: /etc/nginx/conf.d/metrics.conf
      - file: /etc/nginx/snippets/metrics.conf
    - value: {{ grains['fqdn'] }}:443
