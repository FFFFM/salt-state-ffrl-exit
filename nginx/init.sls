nginx_packages_purge:
  pkg.purged:
    - pkgs:
      - nginx-extras

{% for dir in [
  '/etc/nginx',
  '/etc/nginx/conf.d',
  '/etc/nginx/sites-available',
  '/etc/nginx/sites-enabled',
  '/var/log/nginx',
  '/etc/nginx/snippets',
  ] %}
{{ dir }}:
  file.directory:
    - user: root
    - group: root
    - makedirs: True
{% endfor %}

nginx:
  pkgrepo.managed:
    - name: "deb http://openresty.org/package/debian {{ grains['oscodename'] }} openresty"
    - key_url: https://openresty.org/package/pubkey.gpg
    - file: /etc/apt/sources.list.d/openresty.list
    - clean_file: true
  pkg.installed:
    - name: openresty
    - pkgs:
      - openresty
      - openresty-opm
      - openresty-pcre
      - openresty-resty
    - require:
      - pkgrepo: nginx
      - pkg: nginx_packages_purge
      - file: /etc/nginx
      - file: /etc/nginx/conf.d
      - file: /etc/nginx/sites-available
      - file: /etc/nginx/sites-enabled
      - file: /var/log/nginx
  service.running:
    - name: openresty
    - enable: True
    - reload: True
    - watch:
      - file: /etc/nginx/sites-available/*
      - file: /etc/nginx/sites-enabled/*
      - file: /etc/nginx/snippets/*
      - file: /etc/nginx/conf.d/*
      - file: /usr/local/openresty/nginx/conf/nginx.conf
    - require:
      - pkg: openresty

openresty_snippets:
  file.symlink:
    - name: /usr/local/openresty/nginx/conf/snippets
    - target: /etc/nginx/snippets
    - force: True
    - require:
      - pkg: openresty

/usr/local/openresty/nginx/conf/nginx.conf:
  file.managed:
    - source: salt://nginx/files/openresty_conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - require:
      - pkg: openresty

opm_nginx-lua-prometheus:
  cmd.run:
    - name: opm get knyar/nginx-lua-prometheus
    - creates: /usr/local/openresty/site/manifest/nginx-lua-prometheus.meta
    - env:
        HOME: /home/freifunk
    - require:
      - pkg: openresty

/etc/nginx/sites-enabled/default:
  file.absent

# default site
{% set cert_path = "/etc/letsencrypt/live/{cn}/fullchain.pem".format(cn=grains['fqdn']) %}
{% set key_path = "/etc/letsencrypt/live/{cn}/privkey.pem".format(cn=grains['fqdn']) %}

{% set cert_exists = salt['file.file_exists'](cert_path) %}

/srv/www/{{ grains['fqdn'] }}/htdocs:
  file.directory:
    - user: www-data
    - group: www-data
    - makedirs: True

/var/log/nginx/{{ grains['fqdn'] }}:
  file.directory:
    - user: www-data
    - group: adm
    - require:
      - file: /var/log/nginx

/etc/nginx/sites-available/{{ grains['fqdn'] }}.conf:
  file.managed:
    - source:
{% if cert_exists %}
      - salt://nginx/files/sites/{{ grains['fqdn'] }}.conf.j2
      {%- if 'kresd' in salt['pillar.get']('roles', []) %}
      - salt://nginx/files/sites/fqdn-kresd.conf.j2
      {% endif %}
      - salt://nginx/files/sites/default-https.conf.j2
{% else %}
      - salt://nginx/files/sites/default-http.conf.j2
{% endif %}
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - context:
        hostname: '{{ grains.fqdn }}'
        hostnames: '{{ grains.fqdn }}'
        ssl_cert_exists: {{ cert_exists }}
        ssl_cert_path: "{{ cert_path }}"
        ssl_key_path: "{{ key_path }}"
        access_log: "/var/log/nginx/{{ grains.fqdn }}/access.log"
        error_log: "/var/log/nginx/{{ grains.fqdn }}/error.log"
    - require:
      - pkg: nginx

/etc/nginx/sites-enabled/{{ grains['fqdn'] }}.conf:
  file.symlink:
    - target: /etc/nginx/sites-available/{{ grains['fqdn'] }}.conf
    - require:
      - file: /etc/nginx/sites-available/{{ grains['fqdn'] }}.conf

/etc/nginx/snippets/gzip.conf:
  file.managed:
    - user: root
    - group: root
    - mode: '0644'
    - source: salt://nginx/files/snippets/gzip.conf
    - require:
      - file: /etc/nginx/snippets
      - pkg: nginx

/etc/nginx/conf.d/hashes_size.conf:
  file.managed:
    - source: salt://nginx/files/conf.d/hashes_size.conf
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - pkg: nginx

/etc/nginx/conf.d/performance_security.conf:
  file.managed:
    - source: salt://nginx/files/conf.d/performance_security.conf
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - pkg: nginx

/etc/nginx/conf.d/log.conf:
  file.managed:
    - source: salt://nginx/files/conf.d/log.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - require:
      - pkg: nginx

/etc/nginx/conf.d/error.conf:
  file.managed:
    - source: salt://nginx/files/conf.d/error.conf
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - pkg: nginx

/etc/nginx/snippets/error.conf:
  file.managed:
    - source: salt://nginx/files/snippets/error.conf
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - pkg: nginx

/srv/www/default/htdocs/error.html:
  file.managed:
    - user: www-data
    - group: www-data
    - makedirs: True
    - source: salt://nginx/files/error.html

/etc/logrotate.d/nginx-custom:
  file.managed:
    - user: root
    - group: root
    - mode: '0644'
    - source: salt://nginx/files/logrotate
    - require:
      - pkg: nginx

# firewall rule
/etc/ferm/conf.d/40-nginx.conf:
  file.managed:
    - source: salt://nginx/files/ferm.conf
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - pkg: ferm

include:
  - nginx.metrics
{% if 'letsencrypt' in pillar %}
  - common.dhparam
  - nginx.letsencrypt
{% endif %}
  - nginx.vhosts
{% if grains.id == 'www2.aixit.off.de.ffffm.net' %}
  - nginx.sites.net.freifunk.ffm.tiles
{% endif %}
{% if grains.id == 'www3.aixit.off.de.ffffm.net' %}
  - nginx.sites.net.ffffm.firmware
{% endif %}
