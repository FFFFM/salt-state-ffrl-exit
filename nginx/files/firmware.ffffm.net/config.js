/*
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

var config = {
  // list images on console that match no model
  listMissingImages: false,
  // see devices.js for different vendor model maps
  vendormodels: vendormodels,
  // set enabled categories of devices (see devices.js)
  enabled_device_categories: ["recommended"],
  // community prefix of the firmware images
  community_prefix: 'gluon-ffffm-',
  // firmware version regex
  version_regex: /-((v[0-9]+\.[0-9]+(\.[0-9])?)-((stable-[0-9]{4,8})|((rc|next|experimental)-[0-9]{4,8}(-[0-9a-z]{1,8})?(-[0-9a-z]{1,8})?)))[.-]/,
  prettyPrintVersionRegex: '(v[0-9]+(.[0-9]+){1,3})',
  // relative image paths and branch
  directories: {
    './images/stable/factory/': 'stable',
    './images/stable/sysupgrade/': 'stable',
    './images/stable/other/': 'stable',
    './images/rc/factory/': 'rc',
    './images/rc/sysupgrade/': 'rc',
    './images/rc/other/': 'rc',
    './images/next/factory/': 'next',
    './images/next/sysupgrade/': 'next',
    './images/next/other/': 'next',
    './images/experimental/factory/': 'experimental',
    './images/experimental/sysupgrade/': 'experimental',
    './images/experimental/other/': 'experimental',
  },
  // page title
  title: 'Freifunk Frankfurt Firmware',
  // branch descriptions shown during selection
  branch_descriptions: {
    stable: 'Gut getestet, zuverlässig und stabil.',
    rc: 'Release Candidate, wird demnächst als Stable veröffentlicht. In der Regel stabil.',
    next: 'Vorabtests neuer Stable-Kandidaten und Features.',
    experimental: 'Ungetestet, automatisch generiert.'
  },
  // recommended branch will be marked during selection
  recommended_branch: 'stable',
  // experimental branches (show a warning for these branches)
  experimental_branches: ['rc','next','experimental'],
};
