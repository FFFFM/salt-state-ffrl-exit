location /.well-known/acme-challenge {
  root /srv/letsencrypt;
  default_type text/plain;
}
