{% import 'restic/init.sls' as restic %}

firmware:
  user.present:
    - shell: /bin/sh

/home/firmware/.ssh/authorized_keys:
  file.managed:
    - makedirs: True
    - user: firmware
    - group: firmware
    - mode: '0600'
    - require:
      - user: firmware
    - contents: |
      {%- for key in pillar['firmware']['keys'] %}
        {{ key }}
      {%- endfor %}

/home/firmware/gluon-firmware-wizard:
  git.latest:
    - name: https://github.com/freifunk-darmstadt/gluon-firmware-wizard.git
    - target: /home/firmware/gluon-firmware-wizard
    - force_fetch: True
    - force_reset: True
    - refspec_branch: master
    - user: firmware
    - require:
      - user: firmware

/home/firmware/gluon-firmware-wizard/config.js:
  file.managed:
    - source: salt://nginx/files/firmware.ffffm.net/config.js
    - user: firmware
    - group: firmware
    - mode: '0644'
    - require:
      - git: /home/firmware/gluon-firmware-wizard

/home/firmware/openlayers:
  file.recurse:
    - source: salt://nginx/files/firmware.ffffm.net/openlayers
    - clean: true
    - user: firmware
    - group: firmware
    - file_mode: '0644'
    - dir_mode: '0755'
    - require:
      - user: firmware

{% for dir in [
  '/home/firmware/images/',
  '/home/firmware/archive/',
  '/home/firmware/archive/stable/',
  '/home/firmware/modules/',
] %}
{{ dir }}:
  file.directory:
    - user: firmware
    - group: firmware
    - require:
      - user: firmware
{% endfor %}

{{ restic.path('firmware', '/home/firmware/archive/stable/', '/home/firmware/modules/', '/home/firmware/images/') }}

/srv/www/firmware.ffffm.net/htdocs:
  file.symlink:
    - target: /home/firmware/gluon-firmware-wizard
    - require:
      - git: /home/firmware/gluon-firmware-wizard
