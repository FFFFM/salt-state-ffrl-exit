{% set docker = salt['pillar.get']('docker', {}) %}
{% set volume = docker.volume_root | default('/srv/docker') ~ '/cadvisor' %}
{% set image = 'gcr.io/cadvisor/cadvisor:v0.47.2' %}
{% set cadvisor = salt['pillar.get']('cadvisor', {}) %}
{% set metrics = cadvisor.get('metrics', {}) %}
{% set port = '9999' %}

include:
  - docker

/etc/ferm/conf.d/40-cadvisor.conf:
  file.absent: []

docker-image-{{ image }}:
  docker_image.present:
    - name: {{ image }}
    - force: true
    - require:
      - pkg: docker

docker-container-cadvisor:
  docker_container.running:
    - image: {{ image }}
    - name: cadvisor
    - command: all
    - restart_policy: always
    - mem_limit: 150M
    - port_bindings:
      - "{{ port }}:8080"
    - binds:
      - /:/rootfs:ro
      - /var/run:/var/run:ro
      - /sys:/sys:ro
      - /var/lib/docker/:/var/lib/docker:ro
      - /dev/disk/:/dev/disk:ro
    - devices:
      - /dev/kmsg
    - require:
      - docker_image: docker-image-{{ image }}
    - watch:
      - docker_image: docker-image-{{ image }}

prometheus_cadvisor_export:
  grains.present:
    - value: {{ grains['fqdn'] }}:{{ port }}
