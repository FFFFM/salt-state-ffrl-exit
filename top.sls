{{ saltenv }}:
  '*':
    - ssh
    - apt
    - ferm
    - salt.minion
    - mosh
    - common.packages
    - common.datetime
    - common.dotfiles
    - common.locales
    - common.userfreifunk
    - common.ffadmin
    - common.sudo
    - common.bpfcc
    - users
    - systemd
    - ntp
    - prometheus
    - restic
    - logrotate
    - network
  'test.ffm.freifunk.net':
    - lldp
    - gre.tap
  'virtual:physical':
    - match: grain
    - ipmi
    - lldp
  'roles:vmhost':
    - match: pillar
    - ganeti
    - bird
    - bird.bgp
    - bird.ospf
    - kernel
    - kernel.sysctl
  'roles:kresd':
    - match: pillar
    - dnsresolver.kresd
    - bird
    - bird.bgp
    - bird.ospf
    - kernel.sysctl
  'roles:traefik':
    - match: pillar
    - traefik
  'roles:netbox':
    - match: pillar
    - netbox
  'roles:unbound':
    - match: pillar
    - dnsresolver.unbound
    - bird
    - bird.bgp
    - bird.ospf
    - kernel.sysctl
  'roles:gitlab-ci':
    - match: pillar
    - gitlab-ci
    - kernel
    - kernel.sysctl
  'roles:icvpn':
    - match: pillar
    - bird
    - tinc
    - tinc.icvpn
    - icvpn
    - kernel.sysctl
  'roles:dn42':
    - match: pillar
    - bird
    - wireguard
    - wireguard.dn42
    - kernel.sysctl
  'roles:gluon-build':
    - match: pillar
    - gluon-build
  'roles:pretix':
    - match: pillar
    - pretix
  'roles:haproxy':
    - match: pillar
    - haproxy
  'roles:influxdb':
    - match: pillar
    - influxdb
  'roles:jool':
    - match: pillar
    - bird
    - bird.ospf
    - kernel
    - kernel.sysctl
  'roles:yanic':
    - match: pillar
    - yanic
    - nginx
    - letsencrypt
    - kernel.bbr
  'salt.ffm.freifunk.net':
    - salt.master
  'a.ns.as64475.net':
    - knot-dns
  'b.ns.as64475.space':
    - knot-dns
  'c.ns.ffffm.net':
    - knot-dns
  'd.ns.freifunk-frankfurt.de':
    - knot-dns
  'core*.*.*.*.as64475.net':
    - bird
    - bird.bgp
    - bird.ospf
    - kea-dhcp
    - kernel
    - kernel.sysctl
    - gre.tap
    - gre.tun
  'rr*.as64475.net':
    - bird
    - bird.bgp
    - kernel.sysctl
  'www2.aixit.off.de.ffffm.net':
    - nginx
    - letsencrypt
    - meshviewer
    - api
    - grafana
    - kernel
    - kernel.sysctl
  'cms-hessen.aixit.off.de.ffffm.net':
    - nginx
    - letsencrypt
    - kernel
    - kernel.sysctl
  'graphite1.aixit.off.de.ffffm.net':
    - graphite
    - nginx
    - letsencrypt
    - kernel
    - kernel.sysctl
  'prometheus1.aixit.off.de.ffffm.net':
    - letsencrypt
    - nginx
  'www3.aixit.off.de.ffffm.net':
    - nginx
    - letsencrypt
    - docker
    - kernel
    - kernel.sysctl
    - kernel.bbr
  '*.batman15.*.ffffm.net':
    - batman_adv
    - mesh-announce
    - kernel
    - kernel.sysctl
    - network.systemd
  'gw*.batman15.*.ffffm.net':
    - bird
    - bird.bgp
    - bird.ospf
    - fastd
    - kea-dhcp
  'gitlab.aixit.off.de.frickel.cloud':
    - kernel
    - kernel.sysctl
    - kernel.bbr
    - gitlab
    - cpthook-gitlab
  'gateway.aixit.off.de.as64475.net':
    - bird
    - bird.bgp
    - bird.ospf
    - wireguard
    - kernel
    - kernel.sysctl
  'haproxy*.aixit.off.de.ffffm.net':
    - bird
    - bird.bgp
    - bird.ospf
    - kernel
    - kernel.bbr
    - kernel.sysctl
  'cache*.aixit.off.de.ffffm.net':
    - bird
    - bird.bgp
    - bird.ospf
    - kernel
    - kernel.bbr
    - kernel.sysctl
    - varnish
  'director.aixit.off.de.ffffm.net':
    - director
    - letsencrypt
    - nginx
  'lab.esh1.fra.de.frickel.cloud':
    - bird
    - bird.bgp
    - bird.ospf
    - kernel
    - kernel.bbr
    - kernel.sysctl
    - grafana
  'docker1.aixit.fra.de.ffffm.net':
    - bird
    - bird.bgp
    - bird.ospf
    - kernel
    - kernel.bbr
    - kernel.sysctl
    - nginx
    - letsencrypt
    - docker
    - varnish
    - grafana
