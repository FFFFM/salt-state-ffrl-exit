{% import 'restic/init.sls' as restic %}

{% set docker = salt['pillar.get']('docker', {}) %}
{% set grafana = pillar.get('grafana', {}) %}
{% set image_tag = grafana.get('tag', 'latest') %}
{% set plugins = grafana.get('plugins', []) %}
{% set domain = grafana.get('domain', grains.fqdn) %}
{% set volume = docker.volume_root | default('/srv/docker') ~ '/grafana' %}
{% set data_dir = volume ~ '/data' %}
{% set provisioning_dir = volume ~ '/provisioning' %}
{% set compose = docker.volume_root | default('/srv/docker') ~ '/compose' %}

include:
  - docker
  - docker.compose

compose-dir:
  file.directory:
    - name: {{ compose }}
    - makedirs: True
    - require:
      - file: docker-volume-root

docker-volume-grafana:
  file.directory:
    - name: {{ volume }}
    - makedirs: True
    - require:
      - file: docker-volume-root

{{ restic.path('grafana', data_dir) }}

docker-volume-grafana-data:
  file.directory:
    - name: {{ data_dir }}
    - makedirs: True
    - user: 472
    - group: 472
    - mode: '0777'
    - require:
      - file: docker-volume-grafana

docker-volume-grafana-provisioning:
  file.directory:
    - name: {{ provisioning_dir }}
    - makedirs: True
    - user: 472
    - group: 472
    - mode: '0777'
    - require:
      - file: docker-volume-grafana

{% if salt['pillar.get']('grafana:provisioning_datasources:datasources') %}
{{ provisioning_dir }}/datasources/import.yaml:
  file.serialize:
    - dataset_pillar: grafana:provisioning_datasources
    - encoding: utf8
    - user: 472
    - group: 472
    - makedirs: True
    - require:
      - file: docker-volume-grafana
    - watch_in:
      - docker-container-grafana
{% endif %}

{{ provisioning_dir }}/dashboards:
  file.recurse:
    - source: salt://grafana/files/dashboards
    - user: 472
    - group: 472
    - makedirs: True
    - require:
      - file: docker-volume-grafana
    - watch_in:
      - docker-container-grafana

docker-image-grafana/grafana-oss:{{ image_tag }}:
  docker_image.present:
    - name: grafana/grafana-oss:{{ image_tag }}
    - force: true

docker-image-grafana-renderer/grafana/grafana-image-renderer:latest:
  docker_image.present:
    - name: grafana/grafana-image-renderer:latest
    - force: true

grafana-net-proxy:
  docker_network.present:
    - name: proxy
    - attachable: true

/etc/varnish/default.vcl:
  file.managed:
    - source:
      - salt://grafana/files/varnish-grafana.vcl.j2
    - makedirs: True
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - context:
        frontend: "{{ grafana.get('frontend', '') }}"

{{ compose }}/docker-compose.yml:
  file.managed:
    - source:
      - salt://grafana/files/docker-compose.yml.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - context:
        admin_password: "{{ grafana.get('admin_password', '') }}"
        domain: "{{ domain }}"
        data_dir: "{{ data_dir }}"
        provisioning_dir: "{{ provisioning_dir }}"
        root_url: "{{ grafana.get('external-url') }}"
        plugins: "{{ plugins|join(',') }}"
        frontend: "{{ grafana.get('frontend', '') }}"
        image_tag: "{{ image_tag }}"
        grafana_prefix_v6: {{ docker.ipv6.grafana }}
    - require:
      - docker_image: docker-image-grafana/grafana-oss:{{ image_tag }}
      - docker_image: docker-image-grafana-renderer/grafana/grafana-image-renderer:latest
      - file: docker-volume-grafana-data
      - file: docker-volume-grafana-provisioning
      - docker_network: grafana-net-proxy

{%- if grafana.get('frontend', '') == 'traefik' %}
docker_compose_restart_onchanges_varnish:
  cmd.run:
    - name: /usr/local/bin/docker-compose restart
    - cwd: {{ compose }}
    - onchanges:
      - file: /etc/varnish/default.vcl
    - require:
      - file: /etc/varnish/default.vcl
      - file: docker-volume-grafana-data
      - file: docker-volume-grafana-provisioning
      - docker_network: grafana-net-proxy
{%- elif grafana.get('frontend', '') == 'nginx' %}
restart-varnish:
  service.running:
    - name: varnish
    - watch:
      - file: /etc/varnish/default.vcl
{%- endif %}

docker_compose_up:
  cmd.run:
    - name: /usr/local/bin/docker-compose up -d
    - cwd: {{ compose }}
    - require:
      - file: docker-volume-grafana-data
      - file: docker-volume-grafana-provisioning
      - docker_network: grafana-net-proxy

prometheus_grafana_export:
  grains.present:
    - value: "{{ domain }}:443"
    - require:
      - file: {{ compose }}/docker-compose.yml
      - cmd: docker_compose_up
