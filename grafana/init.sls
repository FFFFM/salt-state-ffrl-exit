{%- if 'grafana' in salt['pillar.get']('roles', []) %}
include:
  - grafana.grafana
{%- if  pillar.get('grafana', {}).get('frontend', '') == 'nginx' %}
  - grafana.varnish
{%- endif %}
{%- else %}
grafana:
  pkgrepo.absent: []
  pkg.purged: []
  file.absent:
    - name: /etc/apt/sources.list.d/grafana.list
{%- endif %}
