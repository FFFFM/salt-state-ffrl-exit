disable-root-login:
  cmd.run:
    - name: "passwd -l root"
    - unless: 'passwd -S root | cut -d " " -f2 | grep L > /dev/null'
