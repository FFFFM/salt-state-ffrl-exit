# Restic
This module backups the provided data to a [Restic](https://restic.readthedocs.io/en/latest/)
[REST Server](https://restic.readthedocs.io/en/latest/030_preparing_a_new_repo.html#rest-server) over https.

When no backup paths are specified, `/dev/null` is added to allow for the same rollout on every system.


## Requirements
* Python
  * `jmespath`
* Parameters
  * The `cmd` command must start with a command which has to be an absolute path to a binary or exist in `$PATH`.


## Pillar
Pillar configuration to configure the backup target

```yaml
restic:
  http_user: {{ grains['id'] }}
  http_password: 1anDoMpAs5Wo1D
  http_host: restic.host.tld
  password: rAnD0MpA5SWoRd # The repository password
  backup:
    cmd: # Optional, commands to run before starting the backup
      - echo 'I should be in the backup' > /tmp/note
    path: # Paths to include in the backup
      - /tmp/note
      - /etc/
    exclude: # Paths to exclude from backup
      - /etc/foo/
  time: 5:42 # Optional, time to start the backup, default 02:00
  delay: 300 # Optional, in seconds, defaults 2h
```


## States
Include backups by other states

```jinja
{% import 'restic/init.sls' as restic %}

[...]

{{ restic.cmd('cmd name', '/usr/local/bin/dump-data > /some/file') }}
{{ restic.path('path name', '/some/file') }}
{{ restic.exclude('exclude name', '/another/path') }}
```

