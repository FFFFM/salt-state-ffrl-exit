{% set restic = pillar.get('restic', {}) -%}
{% set backup = restic.get('backup', {}) %}

restic:
  pkg.installed: []

restic password:
  file.managed:
    - name: /etc/restic.password
    - user: root
    - group: root
    - mode: '0600'
    - contents: {{ restic.get('password') }}

{% set repository = ['rest:https://', restic.get('http_user'), ':', restic.get('http_password'), '@',  restic.get('http_host'), '/', restic.get('http_user')] | join %}
restic init:
  cmd.run:
    - env:
      - RESTIC_REPOSITORY: {{ repository }}
      - RESTIC_PASSWORD_FILE: /etc/restic.password
    - unless: restic snapshots -r '{{ repository }}' -p /etc/restic.password
    - require:
      - file: restic password
      - pkg: restic

/etc/systemd/system/restic.service:
  file.managed:
    - source: salt://restic/files/restic.service.j2
    - template: jinja
    - require:
      - pkg: restic
  service.enabled:
    - name: restic
    - require:
      - file: /etc/systemd/system/restic.service

/etc/systemd/system/restic.timer:
  file.managed:
    - source: salt://restic/files/restic.timer.j2
    - template: jinja
  service.running:
    - name: restic.timer
    - enable: True
    - require:
      - service: restic
      - file: restic password
      - cmd: restic init

{% macro cmd(name) %}
restic run {{ name }}:
  file.accumulated:
    - name: restic.cmd
    - text: {{ varargs | list | tojson }}
    - filename: /etc/systemd/system/restic.service
    - require_in:
      - file: /etc/systemd/system/restic.service
{% endmacro %}

{% macro path(name) %}
restic backup {{ name }}:
  file.accumulated:
    - name: restic.path
    - filename: /etc/systemd/system/restic.service
    - text: {{ varargs | list | tojson }}
    - require_in:
      - file: /etc/systemd/system/restic.service
{% endmacro %}

{% macro exclude(name) %}
restic exclude {{ name }}:
  file.accumulated:
    - name: restic.exclude
    - filename: /etc/systemd/system/restic.service
    - text: {{ varargs | list | tojson }}
    - require_in:
      - file: /etc/systemd/system/restic.service
{% endmacro %}

{% if backup.get('cmd', []) %}
{{ cmd('defaults', *backup.get('cmd', [])) }}
{% endif %}
{% if backup.get('path', []) %}
{{ path('defaults', *backup.get('path', [])) }}
{% endif %}
{% if backup.get('exclude', []) %}
{{ exclude('defaults', *backup.get('exclude', [])) }}
{% endif %}

