gluon-build-dependencies:
  pkg.latest:
    - pkgs:
      - git
      - subversion
      - build-essential
      - gawk
      - zip
      - unzip
      - zlib1g-dev
      - libncurses5-dev
      - libssl-dev
      - curl
      - wget
      - ecdsautils
      - time
      - file
