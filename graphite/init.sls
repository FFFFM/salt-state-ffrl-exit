{%- set includes = [] %}
{%- if 'go-carbon' in salt['pillar.get']('roles') %}
  {%- do includes.append('.go-carbon') %}
{%- endif %}
{%- if 'carbonapi' in salt['pillar.get']('roles') %}
  {%- do includes.append('.carbonapi') %}
{%- endif %}

{%- if includes|length > 0 %}
include: {{ includes | yaml }}
{%- endif %}
