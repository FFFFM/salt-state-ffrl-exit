{%- from 'prometheus/map.jinja' import prometheus with context -%}
include:
  - ferm

prometheus-node-exporter-pkg:
  pkg.removed:
    - pkgs:
      - prometheus-node-exporter

/etc/init.d/prometheus-node-exporter:
  file.absent

/lib/systemd/system/prometheus-node-exporter.service:
  file.absent

prometheus-node-exporter-tarball:
  archive.extracted:
    - source: https://github.com/prometheus/node_exporter/releases/download/v{{ prometheus['node_exporter_release'] }}/node_exporter-{{ prometheus['node_exporter_release'] }}.linux-amd64.tar.gz
    - source_hash: https://github.com/prometheus/node_exporter/releases/download/v{{ prometheus['node_exporter_release'] }}/sha256sums.txt
    - if_missing: /opt/node_exporter-{{ prometheus['node_exporter_release'] }}.linux-amd64
    - name: /opt
    - user: root
    - group: root
    - watch_in:
      - service: prometheus-node-exporter.service
    - require_in:
      - service: prometheus-node-exporter.service

prometheus-node-exporter.service:
  service.running:
    - enable: True

/etc/systemd/system/prometheus-node-exporter.service:
  file.managed:
    - source: salt://prometheus/files/node-exporter/prometheus-node-exporter.service.j2
    - template: jinja
    - follow_symlinks: False
    - require:
      - archive: prometheus-node-exporter-tarball
    - watch_in:
      - service: prometheus-node-exporter.service
    - require_in:
      - service: prometheus-node-exporter.service
  module.run:
    - name: service.systemctl_reload
    - onchanges:
      - file: /etc/systemd/system/prometheus-node-exporter.service
    - watch_in:
      - service: prometheus-node-exporter.service
    - require_in:
      - service: prometheus-node-exporter.service

/etc/ferm/conf.d/40-prometheus.conf:
  file.managed:
    - source: salt://prometheus/files/node-exporter/ferm.conf
    - user: root
    - group: root
    - mode: '0644'
    - require:
      - pkg: ferm

/etc/default/prometheus-node-exporter:
  file.managed:
    - source: salt://prometheus/files/node-exporter/prometheus-node-exporter
    - user: root
    - group: root
    - mode: '0644'
    - watch_in:
      - service: prometheus-node-exporter.service
    - require_in:
      - service: prometheus-node-exporter.service

/etc/tmpfiles.d/prometheus-node-exporter-textfiles.conf:
  file.managed:
    - contents: |
          #Type Path                                     Mode UID  GID  Age Argument
          d     /run/prometheus-node-exporter/textfiles/ 0755 root root -   -
    - user: root
    - group: root
    - mode: '0644'

reload tmpfiles:
  cmd.run:
    - name: systemd-tmpfiles --create
    - onchanges:
      - file: /etc/tmpfiles.d/prometheus-node-exporter-textfiles.conf
