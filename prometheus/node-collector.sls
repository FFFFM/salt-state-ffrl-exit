moreutils:
  pkg.installed

python3-prometheus-client:
  pkg.installed

https://frickel.cloud/prometheus-community/node-exporter-textfile-collector-scripts.git:
  git.latest:
    - target: /opt/node-cron-collector
    - force_fetch: true
    - force_reset: true

apt-collector-cron:
  cron.present:
    - name: '/opt/node-cron-collector/apt.sh | sponge /run/prometheus-node-exporter/textfiles/apt.prom'
    - minute: '*/5'
    - require:
      - git: https://frickel.cloud/prometheus-community/node-exporter-textfile-collector-scripts.git
      - pkg: moreutils

chrony-collector-cron:
  cron.present:
    - name: '/opt/node-cron-collector/chrony.py | sponge /run/prometheus-node-exporter/textfiles/chrony.prom'
    - minute: '*'
    - require:
      - git: https://frickel.cloud/prometheus-community/node-exporter-textfile-collector-scripts.git
      - pkg: python3-prometheus-client

{% if grains['virtual'] == 'physical' %}
smart_metrics-collector-cron:
  cron.present:
    - name: '/opt/node-cron-collector/smartmon.sh | sponge /run/prometheus-node-exporter/textfiles/smart_metrics.prom'
    - minute: '*/5'
    - require:
      - git: https://frickel.cloud/prometheus-community/node-exporter-textfile-collector-scripts.git
      - pkg: moreutils
{% endif %}
