{% import 'restic/init.sls' as restic %}
{%- from 'prometheus/map.jinja' import prometheus with context -%}

prometheus tarball:
  archive.extracted:
    - source: https://github.com/prometheus/prometheus/releases/download/v{{ prometheus['release'] }}/prometheus-{{ prometheus['release'] }}.linux-amd64.tar.gz
    - source_hash: https://github.com/prometheus/prometheus/releases/download/v{{ prometheus['release'] }}/sha256sums.txt
    - if_missing: /opt/prometheus-{{ prometheus['release'] }}.linux-amd64
    - name: /opt
    - user: root
    - group: root
    - watch_in:
      - service: prometheus.service
    - require_in:
      - service: prometheus.service

{{ restic.path('prometheus', '/metrics/') }}

/etc/prometheus:
  file.directory:
    - watch_in:
      - service: prometheus.service
    - require_in:
      - service: prometheus.service

/etc/prometheus/rules:
  file.recurse:
    - source: salt://prometheus/files/prometheus/rules
    - file_mode: '0644'
    - user: root
    - group: root
    - dir_mode: '0755'
    - watch_in:
      - service: prometheus.service
    - require_in:
      - service: prometheus.service

/etc/prometheus/targets:
  file.recurse:
    - source: salt://prometheus/files/prometheus/targets
    - file_mode: '0644'
    - user: root
    - group: root
    - dir_mode: '0755'
    - template: jinja
    - require_in:
      - service: prometheus.service


{% set pretix = salt['pillar.get']('pretix', {}) -%}
{% set pretix_metrics = pretix.get('metrics', {}) -%}
/etc/prometheus/pretix_password_file.txt:
  file.managed:
    - contents: {{ pretix_metrics.get('password') }}
    - mode: '0644'
    - user: root
    - group: root

/etc/prometheus/prometheus.yml:
  file.managed:
    - source: salt://prometheus/files/prometheus/prometheus.yml
    - user: root
    - group: root
    - mode: '0644'
    - watch_in:
      - service: prometheus.service
    - require_in:
      - service: prometheus.service

/etc/default/prometheus:
  file.managed:
    - source: salt://prometheus/files/prometheus/prometheus
    - user: root
    - group: root
    - mode: '0644'
    - watch_in:
      - service: prometheus.service
    - require_in:
      - service: prometheus.service

/etc/systemd/system/prometheus.service:
  file.managed:
    - source: salt://prometheus/files/prometheus/prometheus.service.j2
    - template: jinja
    - require:
      - archive: prometheus tarball
    - watch_in:
      - service: prometheus.service
    - require_in:
      - service: prometheus.service

prometheus.service:
  service.running:
    - enable: True
