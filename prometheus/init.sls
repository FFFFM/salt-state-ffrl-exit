prometheus:
  user.present:
    - home: /var/lib/prometheus
    - shell: /usr/sbin/nologin

include:
  - prometheus.exporter
  - prometheus.checkrestart
  - prometheus.node-collector
{%- if 'prometheus' in salt['pillar.get']('roles', []) %}
  - prometheus.prometheus
  - prometheus.alertmanager
  - prometheus.irc
{%- endif %}
