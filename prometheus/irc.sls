{%- set gopath = salt['pillar.get']('golang:gopath', '/usr/local/go') %}
{%- set gocache = salt['pillar.get']('golang:gocache', '/var/cache/go') %}
{%- set project = 'cpthook' %}
{%- set version = salt['pillar.get']('CptHook:version', 'main') %}
{%- set uri = salt['pillar.get']('CptHook:uri', 'github.com/fleaz/CptHook') %}

include:
  - golang

{{ project }}:
  git.latest:
    - name: https://{{ uri }}
    - target: /usr/src/{{ project }}
    - force_fetch: True
    - force_reset: True
    - rev: {{ version }}
    - refspec_branch: main
  cmd.run:
    - cwd: /usr/src/{{ project }}
    - name: go install -v {{ uri }}
    - env:
        GOPATH: {{ gopath }}
        GOCACHE: {{ gocache }}
        GO111MODULE: "on"
    - require:
      - pkg: golang
      - git: {{ project }}
    - onchanges:
      - pkg: golang
      - git: {{ project }}
  service.running:
    - enable: True
    - require:
      - file: /etc/systemd/system/{{ project }}.service
    - watch:
      - file: /etc/systemd/system/{{ project }}.service
      - file: /etc/{{ project }}.yml
      - cmd: {{ project }}

/etc/systemd/system/{{ project }}.service:
  file.managed:
    - source: salt://prometheus/files/{{ project }}/{{ project }}.service.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - context:
        gopath: {{ gopath }}

/etc/{{ project }}.yml:
  file.managed:
    - source: salt://prometheus/files/{{ project }}/{{ project }}.yml
    - user: root
    - group: root
    - mode: '0644'
