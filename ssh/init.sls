openssh-server:
  pkg.installed:
    - pkgs:
      - openssh-server
  service.running:
    - name: ssh
    - enable: True
    - reload: True
ssh-user:
  group.present:
    - system: True


/etc/ssh/sshd_config:
  file.managed:
    - source:
      - salt://ssh/files/sshd_config.{{ grains.os }}.{{ grains.oscodename }}
      - salt://ssh/files/sshd_config
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja

/etc/ssh/ssh_config:
  file.managed:
    - source:
      - salt://ssh/files/ssh_config.{{ grains.os }}.{{ grains.oscodename }}
      - salt://ssh/files/ssh_config
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja

/etc/issue.net:
  file.managed:
    - source:
      - salt://ssh/files/fire_banner
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja

undesirable-hostkeys:
  file.absent:
    - names:
      - /etc/ssh/ssh_host_dsa_key
      - /etc/ssh/ssh_host_dsa_key.pub
      - /etc/ssh/ssh_host_ecdsa_key
      - /etc/ssh/ssh_host_ecdsa_key.pub
{%- if not 'vmhost' in salt['pillar.get']('roles', []) %}
      - /etc/ssh/ssh_host_rsa_key
      - /etc/ssh/ssh_host_rsa_key.pub
{%- endif %}
