systemd_pkgs:
  pkg.latest:
{%- if grains['osmajorrelease'] >= 9 and grains['osmajorrelease'] <= 12 %}
    - fromrepo: {{ grains['oscodename'] }}-backports
{%- endif %}
    - pkgs:
      - systemd
      - libpam-systemd
      - systemd-coredump

dbus:
  pkg.installed

# systemctl daemon-reload if anything changed
service.systemctl_reload:
  module.run:
    - onchanges:
      - file: /etc/systemd/system/*

include:
  - apt
