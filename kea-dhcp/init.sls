kea-dhcp-repo:
  pkgrepo.managed:
    - name: deb https://dl.cloudsmith.io/public/isc/kea-2-4/deb/{{ grains['os_family'] | lower }} {{ grains['oscodename'] }} main
    - key_url: https://dl.cloudsmith.io/public/isc/kea-2-4/gpg.0D9D9A1439E23DB9.key
    - file: /etc/apt/sources.list.d/kea.list
    - clean_file: True


kea-cleanup:
  pkg.removed:
    - pkgs:
      - kea-dhcp4-server
      - kea-dhcp6-server

kea-dhcp4-server.service:
  service.masked

kea-dhcp6-server.service:
  service.masked

kea-dhcp4-server:
  pkg.latest:
    - pkgs:
      - isc-kea-dhcp4-server
    - require:
      - pkgrepo: kea-dhcp-repo
  service.running:
    - name: isc-kea-dhcp4-server
    - enable: True
    - reload: False
    - sig: kea-dhcp4
    - watch:
      - pkg: kea-dhcp4-server
      - file: /etc/kea/kea-dhcp4.conf
      - file: kea-dhcp4-service-overwrite

/etc/kea/kea-dhcp4.conf:
  file.managed:
    - source: salt://kea-dhcp/files/kea-dhcp4.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - require:
      - pkg: kea-dhcp4-server

kea-dhcp4-service-overwrite:
  file.managed:
    - name: /etc/systemd/system/isc-kea-dhcp4-server.service.d/overwrite.conf
    - source: salt://kea-dhcp/files/overwrite.conf
    - makedirs: True


kea-dhcp6-server:
  pkg.latest:
    - pkgs:
      - isc-kea-dhcp6-server
    - require:
      - pkgrepo: kea-dhcp-repo
  service.running:
    - name: isc-kea-dhcp6-server
    - enable: True
    - reload: False
    - sig: kea-dhcp6
    - watch:
      - pkg: kea-dhcp6-server
      - file: /etc/kea/kea-dhcp6.conf
      - file: kea-dhcp6-service-overwrite

/etc/kea/kea-dhcp6.conf:
  file.managed:
    - source: salt://kea-dhcp/files/kea-dhcp6.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - require:
      - pkg: kea-dhcp6-server

kea-dhcp6-service-overwrite:
  file.managed:
    - name: /etc/systemd/system/isc-kea-dhcp6-server.service.d/overwrite.conf
    - source: salt://kea-dhcp/files/overwrite.conf
    - makedirs: True
