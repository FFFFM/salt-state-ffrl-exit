Example Pillar:

```yaml
kea_dhcp:

  IPv4:
    interfaces:
      - eth1
    subnets:
      - subnet: 192.168.200.0/24
        pools:
          - 192.168.200.10 - 192.168.200.50
        options:
          - name: routers
            data: 192.168.200.5
    options:
      - name: domain-name-servers
        data: 8.8.8.8, 8.8.4.4

  IPv6:
    interfaces:
      - eth1
    subnets:
      - subnet: 2001:db8:1::/64
        pools:
          - 2001:db8:1::/64
      - subnet: 2001:db8::/64
        interface: eth2
        pools:
          - 2001:db8::/64
    options:
      - name: dns-servers
        data: 2001:4860:4860::8888, 2001:4860:4860::8844

domains:
  legacy:
    domain: clients.ffffm.net

    # see: https://lists.isc.org/pipermail/kea-users/2017-February/000871.html
    #search_binary: '066c656761637905666666666d036e6574000366666d086672656966756e6b036e657400'
    search:
      - ffm.freifunk.net

    IPv4:
      subnets:
        10.126.0.0/16:
          ranges:
            - start: 10.126.0.10
              end: 10.126.3.254
              routers:
                - 10.126.0.1
      name_servers:
        - 192.0.2.56
        - 192.0.2.57
    IPv6:
      subnets:
        '2001:db8::/64':
          network: '2001:db8::'
          prefix: 64
      name_servers:
      - 2001:db8::1:56
      - 2001:db8::1:57
```
