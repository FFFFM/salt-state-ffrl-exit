{%- set gopath = salt['pillar.get']('golang:gopath', '/usr/local/go') %}
{%- set gopkg = 'git.darmstadt.ccc.de/ffda/infra/fastd-exporter' %}

include:
  - golang
  - ferm

fastd-exporter:
  git.latest:
    - name: https://{{ gopkg }}.git
    - target: {{ gopath }}/src/{{ gopkg }}
    - rev: {{ salt['pillar.get']('fastd-exporter:rev', 'master') }}
    - force_fetch: True
    - force_reset: remote-changes
  cmd.run:
    - cwd: {{ gopath }}/src/{{ gopkg }}
    - name: go install -v {{ gopkg }}
    - env:
        GOPATH: {{ gopath }}
        GOCACHE: /tmp/GOCACHE/
        GO111MODULE: "on"
    - require:
      - pkg: golang
      - git: fastd-exporter
    - onchanges:
      - git: fastd-exporter
  service.running:
    - enable: True
    - require:
      - file: /etc/systemd/system/fastd-exporter.service
    - watch:
      - file: /etc/systemd/system/fastd-exporter.service
      - cmd: fastd-exporter

/etc/systemd/system/fastd-exporter.service:
  file.managed:
    - source: salt://fastd/files/fastd-exporter.service.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja

/etc/ferm/conf.d/40-fastd-exporter.conf:
  file.managed:
    - source: salt://fastd/files/ferm-fastd-exporter.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - require:
      - file: /etc/ferm/conf.d

prometheus_fastd_export:
  grains.present:
    - value: {{ grains.nodename }}:9281
