include:
  - fastd.instances
  - fastd.exporter

fastd:
  pkg.installed:
    - fromrepo: {{ grains['oscodename'] }}-backports

/etc/apt/preferences.d/backports-fastd:
  file.managed:
    - contents: |
        Package: fastd
        Pin: release n={{ grains['oscodename'] }}-backports
        Pin-Priority: 900

tun:
  kmod.present:
    - persist: True

fastd_disable_generic_autostart:
  file.replace:
    - name: /etc/default/fastd
    - pattern: ^AUTOSTART=(.*)$
    - repl: AUTOSTART="none"
    - require:
      - pkg: fastd

/var/lib/fastd/peer_groups:
  file.directory:
    - makedirs: True

{% for group_name, group in salt['pillar.get']('fastd:peer_groups',{}).items() %}
/var/lib/fastd/peer_groups/{{ group_name }}.git:
  git.latest:
  - name: {{ group['git_remote'] }}
  - rev: {{ group['git_branch'] }}
  - target: /var/lib/fastd/peer_groups/{{ group_name }}
  - require:
    - pkg: git

/etc/cron.d/fastd-peergroup-{{ group_name }}:
  file.managed:
  - source: salt://fastd/files/fastd-peergroup.cron.j2
  - user: root
  - group: root
  - mode: '0644'
  - template: jinja
  - context:
      group: {{ group_name }}

/usr/local/sbin/fastd-peergroup-update-{{ group_name }}:
  file.managed:
  - source: salt://fastd/files/fastd-peergroup-update.j2
  - user: root
  - group: root
  - mode: '0755'
  - template: jinja
  - context:
      group: {{ group_name }}
{% endfor %}
