# for each domain
{%- for domain_key, domain_val in salt['pillar.get']('domains', {}).items() %}
{%- set domain_id = domain_val['domain_id'] %}

# assign fastd configuration
{%- set fastd = salt['pillar.get']('domains:%s:fastd'|format(domain_key)) %}

# open up ports in the firewall
/etc/ferm/conf.d/40-fastd-dom{{ domain_id }}.conf:
  file.managed:
    - source: salt://fastd/files/ferm.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - context:
        domain: {{ domain_key }}
    - require:
      - file: /etc/ferm/conf.d

# and for each instance (varying mtu/port)
{%- for instance in fastd['instances'] %}
{%- set fastd_config_root = '/etc/fastd/dom{}_{}'.format(domain_id, instance['mtu']) %}

# create working directory
{{ fastd_config_root }}:
  file.directory:
    - mode: '0755'
    - makedirs: True

# deploy fastd.conf
{{ fastd_config_root }}/fastd.conf:
  file.managed:
    - source:
      - salt://fastd/files/fastd-{{ domain_id }}.conf.j2
      - salt://fastd/files/fastd-default.conf.j2
    - user: root
    - group: root
    - mode: '0600'
    - template: jinja
    - context:
        domain: {{ domain_key }}
        domain_id: {{ domain_id }}
        port: {{ instance['port'] }}
        mtu: {{ instance['mtu'] }}

# deploy verify
{{ fastd_config_root }}/verify:
  file.managed:
    - source:
      - salt://fastd/files/verify-{{ domain_id }}.j2
      - salt://fastd/files/verify.j2
    - user: root
    - group: root
    - mode: '0755'
    - template: jinja
    - context:
        domain: {{ domain_key }}
        domain_id: {{ domain_id }}
        port: {{ instance['port'] }}
        mtu: {{ instance['mtu'] }}

# enable instances and watch for config changes
fastd@dom{{ domain_id }}_{{ instance['mtu'] }}:
  service.running:
    - enable: True
    - require:
      - pkg: fastd
    - watch:
      - file: {{ fastd_config_root }}/fastd.conf
{%- endfor %}
{%- endfor %}
