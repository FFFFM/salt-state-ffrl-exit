{% set docker = salt['pillar.get']('docker', {}) %}
{% set traefik = salt['pillar.get']('traefik', {}) %}
{% set backend = traefik.get('backend', {}) %}
{% set volume = docker.volume_root | default('/srv/docker') ~ '/traefik' %}
{% set container = traefik.get('container', 'traefik') %}
{% set version = traefik.get('container', '1.7-alpine') %}

include:
  - docker

docker-volume-traefik:
  file.directory:
    - name: {{ volume }}
    - makedirs: True
    - require:
      - file: docker-volume-root

{{ volume }}/traefik.toml:
  file.managed:
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - source: salt://traefik/files/traefik.toml.j2
    - require:
      - file: docker-volume-traefik

{{ volume }}/acme.json:
  file.managed:
    - user: www-data
    - group: www-data
    - mode: '0600'
    - replace: false
    - require:
      - file: docker-volume-traefik

docker-image-{{ container }}:{{ version }}:
  docker_image.present:
    - name: {{ container }}:{{ version }}
    - force: true
    - watch:
      - pkg: docker
    - require:
      - file: docker-volume-root

traefik-net-proxy:
  docker_network.present:
    - name: proxy
    - attachable: true

docker-container-traefik:
  docker_container.running:
    - image: {{ container }}:{{ version }}
    - name: traefik
    - command: --docker
    - restart_policy: always
    - mem_limit: 200M
    - port_bindings:
      - "80:80"
      - "443:443"
    - networks:
      - proxy
    - binds:
      - /var/run/docker.sock:/var/run/docker.sock
      - {{ volume }}/traefik.toml:/traefik.toml
      - {{ volume }}/acme.json:/acme.json
    - require: &traefik-require
      - pkg: docker
      - file: {{ volume }}/traefik.toml
      - file: {{ volume }}/acme.json
      - docker_network: traefik-net-proxy
    - watch: *traefik-require

grains-traefik-metrics-host:
  grains.present:
    - name: traefik-metrics-host
    - value: {{ backend.get('host', grains.fqdn) }}
    - require:
      - docker_container: docker-container-traefik
