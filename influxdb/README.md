# influxdb

Provides an InfluxDB timeseries database server.

## Example pillar

```yaml
influxdb:
  # this is the user salt uses to create new databases
  admin: root
  # create the following databases
  databases:
    - yanic
  # create the following users
  users:
    root:
      password: changeme
      admin: true
      # admin users don't need grants
    yanic:
      password: insecure
      # permissions can be one of (read|write|all)
      grant:
        # database: permission
        yanic: all
    grafana:
      password: worrysome
      grant:
        yanic: read
```
