{% import 'restic/init.sls' as restic %}

include:
  - ferm
  - common.python

salt-pip-influxdb:
  pip.installed:
    - name: influxdb

python-influxdb:
  pkg.installed:
    - pkgs:
      - python3-influxdb

python2-influxdb:
  pkg.purged:
    - pkgs:
      - python-influxdb

influxdb:
  pkgrepo.managed:
    - humanname: influxdb
    - name: deb https://repos.influxdata.com/{{ grains['os_family'] | lower }} {{ grains['oscodename'] }} stable
    - key_url: https://repos.influxdata.com/influxdata-archive_compat.key
    - file: /etc/apt/sources.list.d/influxdb.list
    - clean_file: True
  pkg.installed:
    - pkgs:
      - influxdb
    - require:
      - pkgrepo: influxdb

/etc/influxdb/influxdb.conf:
  file.managed:
    - source: salt://influxdb/files/influxdb.conf
    - require:
      - pkg: influxdb

/etc/ferm/conf.d/40-influxdb.conf:
  file.managed:
    - source: salt://influxdb/files/ferm.conf.j2
    - template: jinja
    - require:
      - file: /etc/ferm/conf.d

/var/lib/influxdb:
  file.directory:
    - user: influxdb
    - group: influxdb
    - require:
      - pkg: influxdb

/etc/systemd/system/influxdb.service:
  file.managed:
    - source: salt://influxdb/files/influxdb.service
    - require:
      - pkg: influxdb
      - file: /var/lib/influxdb

influxdb.service:
  service.running:
    - enable: True
    - require:
      - file: /etc/systemd/system/influxdb.service
      - file: /var/lib/influxdb
    - watch:
      - file: /etc/influxdb/influxdb.conf


{% for name, user in salt['pillar.get']('influxdb:users').items() %}
influxdb_user_{{ name }}:
  influxdb_user.present:
    - name: {{ name }}
    - passwd: {{ user['password'] }}
    - admin: {{ user.get('admin', False) }}
    {%- if 'grants' in user %}
    - grants:
        {{ user.get('grants', {}) | yaml }}
    {%- endif %}
    - require:
      - service: influxdb.service
      - pkg: python-influxdb
{% endfor %}

{% for name in salt['pillar.get']('influxdb:databases') %}
influxdb_database_{{ name }}:
  influxdb_database.present:
    - name: {{ name }}
    - require:
      - service: influxdb.service
      - pkg: python-influxdb
{% endfor %}

{# We should use the explicit backup here but the host has a small disk #}
{{ restic.path('influxdb', '/var/lib/influxdb/') }}
{# { restic.cmd('influxdb', 'influxd backup -portable /var/lib/influxdb/backup') } #}
{# { restic.path('influxdb', '/var/lib/influxdb/backup') } #}
