{% import 'restic/init.sls' as restic %}

{% set docker = salt['pillar.get']('docker', {}) %}
{% set netbox = salt['pillar.get']('netbox', {}) %}
{% set volume = docker.volume_root | default('/srv/docker') ~ '/netbox' %}
{% set database = netbox.get('database', {}) %}
{% set email = netbox.get('email', {}) %}
{% set napalm = netbox.get('napalm', {}) %}
{% set redis = netbox.get('redis', {}) %}
{% set superuser = netbox.get('superuser', {}) %}
{% set images = netbox.get('images', {}) %}
{% set versions = {
        'netbox': 'netboxcommunity/netbox:' ~ images.get('netbox', 'latest'),
        'nginx': 'nginx:' ~ images.get('nginx', 'stable-alpine'),
        'postgres': 'postgres:' ~ images.get('postgres', '13-alpine'),
        'redis': 'redis:' ~ images.get('redis', '7-alpine')
} %}

include:
  - docker
  - letsencrypt
  - nginx

docker-volume-netbox:
  file.directory:
    - name: {{ volume }}
    - makedirs: True
    - require:
      - file: docker-volume-root

{{ volume }}/media-files:
  file.directory:
    - require:
      - file: docker-volume-netbox

{{ volume }}/db:
  file.directory:
    - user: 70
    - group: 999
    - require:
      - file: docker-volume-netbox

{{ volume }}/redis:
  file.directory:
    - require:
      - file: docker-volume-netbox

{{ volume }}/reports:
  file.directory:
    - require:
      - file: docker-volume-netbox

{{ restic.cmd('docker netbox', 'docker exec -t netbox-db sh -c \'PGPASSWORD="$POSTGRES_PASSWORD" pg_dump -U "$POSTGRES_USER" -d "$POSTGRES_DB" > /var/lib/postgresql/data/dump.sql\'') }}
{{ restic.path('docker netbox', volume) }}

{% for image in versions.values() %}
docker-image-netbox-{{ image }}:
  docker_image.present:
    - name: {{ image }}
    - force: true
    - watch:
      - pkg: docker
    - require:
      - file: docker-volume-root
{% endfor %}

docker-container-netbox-db:
  docker_container.running:
    - image: {{ versions.postgres }}
    - name: &netbox-db-host netbox-db
    - restart_policy: always
    - mem_limit: 500M
    - environment:
        POSTGRES_DB: &netbox-db-table {{ database.get('table', 'netbox') }}
        POSTGRES_USER: &netbox-db-user {{ database.get('user', 'netbox') }}
        POSTGRES_PASSWORD: &netbox-db-password {{ database.get('password', 'netbox') }}
    - binds:
      - {{ volume }}/db:/var/lib/postgresql/data
    - require: &netbox-db-require
      - pkg: docker
      - docker_image: docker-image-netbox-{{ versions.postgres }}
      - file: {{ volume }}/db
    - watch: *netbox-db-require

docker-container-netbox-redis:
  docker_container.running:
    - image: {{ versions.redis }}
    - name: &netbox-redis-host netbox-redis
    - restart_policy: always
    - mem_limit: 500M
    - command: sh -c 'redis-server --appendonly yes --requirepass $REDIS_PASSWORD'
    - environment:
        REDIS_PASSWORD: &netbox-redis-password {{ redis.get('password', 'netbox') }}
    - binds:
      - {{ volume }}/redis:/data
    - require: &netbox-redis-require
      - pkg: docker
      - docker_image: docker-image-netbox-{{ versions.redis }}
      - file: {{ volume }}/redis
    - watch: *netbox-redis-require

docker-container-netbox:
  docker_container.running: &netbox
    - image: {{ versions.netbox }}
    - name: netbox
    - restart_policy: always
    - mem_limit: 1G
    - environment: &netbox-env
        # https://github.com/netbox-community/netbox/blob/v2.8.6/netbox/netbox/settings.py
        ALLOWED_HOSTS: localhost netbox netbox-proxy {{ netbox.host }} 127.0.0.1 ::1
        {% if netbox.get('login-required', true) %}
        LOGIN_REQUIRED: "true"
        {% endif %}
        {% if netbox.get('maintenance', false) %}
        MAINTENANCE_MODE: "true"
        {% endif %}
        SECRET_KEY: {{ netbox.get('secret', '') }}
        DB_HOST: *netbox-db-host
        DB_NAME: *netbox-db-table
        DB_PASSWORD: *netbox-db-password
        DB_USER: *netbox-db-user
        EMAIL_FROM: {{ email.get('from', 'netbox@netbox') }}
        EMAIl_SERVER: {{ email.get('server', 'localhost') }}
        EMAIL_PORT: {{ email.get('port', 25) }}
        EMAIL_PASSWORD: {{ email.get('password', '') }}
        EMAIL_USERNAME: {{ email.get('username', '') }}
        EMAIL_TIMEOUT: {{ email.get('timeout', 10) }}
        NAPALM_USERNAME: {{ napalm.get('user', '') }}
        NAPALM_PASSWORD: {{ napalm.get('password', '') }}
        SUPERUSER_EMAIL: {{ superuser.get('email', 'superuser@netbox') }}
        SUPERUSER_NAME: {{ superuser.get('name', 'superuser') }}
        SUPERUSER_PASSWORD: {{ superuser.get('password', '') }}
        REDIS_HOST: *netbox-redis-host
        REDIS_PASSWORD: *netbox-redis-password
        TASKS_REDIS_HOST: *netbox-redis-host
        TASKS_REDIS_PASSWORD: *netbox-redis-password
        TASKS_REDIS_DATABASE: 2
        CACHING_REDIS_HOST: *netbox-redis-host
        CACHING_REDIS_PASSWORD: *netbox-redis-password
        CACHING_REDIS_DATABASE: 3
        METRICS_ENABLED: "true"
    - links:
      - netbox-db:netbox-db
      - netbox-redis:netbox-redis
      - netbox-worker:netbox-worker
    - port_bindings:
      - '127.0.0.1:8001:8080'
    - binds: &netbox-binds
      - {{ volume }}/media-files:/opt/netbox/netbox/media
      - {{ volume }}/reports:/etc/netbox/reports
    - require: &netbox-require
      - pkg: docker
      - docker_image: docker-image-netbox-{{ versions.netbox }}
      - docker_container: docker-container-netbox-db
      - docker_container: docker-container-netbox-worker
      - file: {{ volume }}/media-files
      - file: {{ volume }}/reports
    - watch: *netbox-require

docker-container-netbox-worker:
  docker_container.running:
    - image: {{ versions.netbox }}
    - name: netbox-worker
    - restart_policy: always
    - mem_limit: 500M
    - entrypoint:
      - /opt/netbox/venv/bin/python
      - /opt/netbox/netbox/manage.py
    - command: "rqworker"
    - environment: *netbox-env
    - links:
      - netbox-db:netbox-db
      - netbox-redis:netbox-redis
    - binds: *netbox-binds
    - require: &netbox-worker-require
      - pkg: docker
      - docker_image: docker-image-netbox-{{ versions.netbox }}
      - docker_container: docker-container-netbox-db
      - file: {{ volume }}/media-files
      - file: {{ volume }}/reports
    - watch: *netbox-worker-require

docker-container-netbox-proxy:
  docker_container.absent:
    - name: netbox-proxy

prometheus_netbox_export:
  grains.present:
    - value: {{ netbox.host }}:443
    - require:
      - docker_container: docker-container-netbox
