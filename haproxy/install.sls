{%- from 'haproxy/map.jinja' import haproxy with context %}

haproxy:
  pkgrepo.managed:
    - humanname: haproxy
    - name: {{ haproxy.apt_repo }}
    - key_url: {{ haproxy.key_url }}
    - file: /etc/apt/sources.list.d/haproxy.list
    - clean_file: True
    - require_in:
      - pkg: haproxy
  pkg.installed:
    - version: {{ haproxy.version }}
  service.running:
    - enable: True
