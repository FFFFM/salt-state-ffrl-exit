{% set docker = salt['pillar.get']('docker', {}) %}
{% set images = docker.get('images', {}) %}


{% for name, config  in images.items() %}
docker-image-{{ name }}{% if 'tag' in config %}:{{ config['tag'] }}{% endif %}:
  docker_image.present:
    - name: {{ name }}{% if 'tag' in config %}:{{ config['tag'] }}{% endif %}
    - force: true
    - watch:
      - pkg: docker
    - require:
      - file: docker-volume-root
{% endfor %}
