---
include:
  - common.python
  - docker.images

docker-compose:
  virtualenv.managed:
    - name: /opt/docker-compose
    - python: /usr/bin/python3
    - require:
      - pkg: python3-virtualenv
    - pip_upgrade: True
    - pip_pkgs:
      - docker-compose

/usr/local/bin/docker-compose:
  file.symlink:
    - target: /opt/docker-compose/bin/docker-compose
    - require:
      - virtualenv: docker-compose
