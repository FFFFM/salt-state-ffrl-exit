{% import 'restic/init.sls' as restic %}

{% set docker = salt['pillar.get']('docker', {}) %}
{% set images = docker.get('images', {}) %}
{% set containers = docker.get('containers', []) %}


{% for container in containers %}
{% set name = container['name'] %}
{% set config = container %}
{% set volumes = config.get('volumes', {}) %}
{% set baseVolume = {'exists': false} %} # Workaround because variables can't be changed inside a for scope

# Only create a volume if its not a file system path
{% for volume, conf in volumes.items() if not volume.startswith('/') %}
{% set backup = conf.get('backup', []) %}

{% if not baseVolume.exists %}
{% do baseVolume.update({'exists': true}) %}
docker-volume-{{ name }}:
  file.directory:
    - name: {{ docker['volume_root'] }}/{{ name }}
    - makedirs: True
    - require:
      - file: docker-volume-root
{% endif %}

docker-volume-{{ name }}-{{ volume }}:
  file.directory:
    - name: {{ docker['volume_root'] }}/{{ name }}/{{ volume }}
    - makedirs: True
    {% if 'user' in conf %}
    - user: {{ conf.user }}
    {% endif %}
    {% if 'group' in conf %}
    - group: {{ conf.group }}
    {% endif %}
    - require:
      - file: docker-volume-{{ name }}

{% if backup != false %}
{% set backup_name = 'docker ' ~ name ~ ' ' ~ volume %}
{{ restic.path(backup_name, dir) }}
{% if 'cmd' in backup %}
{{ restic.cmd(backup_name, backup.cmd) }}
{% endif %}
{% if 'exclude' in backup %}
{{ restic.exclude(backup_name, backup.exclude) }}
{% endif %}
{% endif %}

{% endfor %}

docker-container-{{ name }}:
  docker_container.running:
    - image: {{ config['image'] }}
    - name: {{ name }}
    - restart_policy: {{ config.get('restart', 'always') }}
    - watch:
      - docker_image: docker-image-{{ config['image'] }}
    - require:
      - docker_image: docker-image-{{ config['image'] }}
    {% if volumes %}
    {% for volume in volumes.keys() if not volume.startswith('/') %}
      - file: docker-volume-{{ name }}-{{ volume }}
    {% endfor %}
    {% endif %}
    {% for link in config.get('links',[]) %}
      - docker_container: docker-container-{{ link.split(':')[0] }}
    {% endfor %}
    {% if 'environment' in config %}
    - environment:
      {% for key, value in config['environment'].items() %}
      - {{ key }}: "{{ value }}"
      {% endfor %}
    {% endif %}
    {% if volumes %}
    - binds:
    {% for volume, volume_conf in volumes.items() %}
      - {% if not volume.startswith('/') %}{{ docker['volume_root'] }}/{{ name }}/{% endif %}{{ volume }}:{{ volume_conf['destination'] }}{% if volume_conf.get('read_only', False) %}:ro{% endif %}
    {% endfor %}
    {% endif %}
    {% if 'ports' in config %}
    - port_bindings:
      {% for proto, ports in config['ports'].items() %}
      {% for inner, outer in ports.items() %}
      - "{{ inner }}:{{ outer }}/{{ proto }}"
      {% endfor %}
      {% endfor %}
    {% endif %}
    {% if 'links' in config %}
    - links:
      {% for link in config['links'] %}
      - "{{ link }}{% if ':' not in link %}:{{ link }}{% endif %}"
      {% endfor %}
    {% endif %}
    {% if 'command' in config %}
    - command: "{{ config.command }}"
    {% endif %}
    {% if 'entrypoint' in config %}
    - entrypoint: "{{ config.entrypoint }}"
    {% endif %}
    {% if 'network' in config %}
    - network_mode: "{{ config.network }}"
    {% endif %}
    {% if 'privileged' in config %}
    - privileged: {{ config.privileged }}
    {% endif %}
    {% if 'capabilities' in config %}
    - cap_add: {{ config.capabilities }}
    {% endif %}
    {% if 'capabilities-drop' in config %}
    - cap_drop: {{ config.get('capabilities-drop') }}
    {% endif %}
    {% if 'interactive' in config %}
    - interactive: {{ config.interactive }}
    {% endif %}
    {% if 'labels' in config %}
    - labels: {{ config.labels }}
    {% endif %}
{% endfor %}
