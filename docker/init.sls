{%- from 'docker/map.jinja' import docker with context %}
{% set mirrors = ['http://localhost:5500'] + salt['pillar.get']('docker-mirrors', []) %}

salt-pip-docker:
  pip.installed:
    - name: docker

include:
  - ferm
  - .cache
  - .images
  - .container
  - cadvisor

docker:
  pkgrepo.managed:
    - humanname: docker
    - name: {{ docker.apt_repo }}
    - file: /etc/apt/sources.list.d/docker.list
    - clean_file: True
    - key_url: salt://docker/files/docker.gpg
  pkg.installed:
    - pkgs:
      - docker-ce{% if 'version' in docker %}: {{ docker.version }}{% endif %}
{% if salt['fixes.osmajorrelease']() <= 10 %}
      - python-docker
{% endif %}
      - python3-docker
    - require:
      - pkgrepo: docker
{% if 'version' in docker %}
  file.managed:
    - contents: |
        Explanation: Pin the docker version
        Package: docker-ce
        Pin: version {{ docker.version }}
        Pin-Priority: 1001
{% else %}
  file.absent:
{% endif %}
    - name: /etc/apt/preferences.d/docker-ce-pin-1001
  service.running:
    - enable: True
    - watch:
      - file: /etc/systemd/system/docker.service.d/override.conf
      - file: /etc/docker/daemon.json
    - require:
      - pkg: docker

/etc/docker/daemon.json:
  file.managed:
    - source: salt://docker/files/daemon-config.json.j2
    - template: jinja
    - user: root
    - group: root
    - mode: '0644'
    - defaults:
        mirrors: {{ mirrors }}
    - require:
      - pkg: docker

/etc/ferm/conf.d/40-docker.conf:
  file.managed:
    - source: salt://docker/files/ferm.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - makedirs: True
    - template: jinja
    - require_in:
      - pkg: ferm

docker-volume-root:
  file.directory:
    - name: {{ docker.volume_root | default('/srv/docker') }}
    - makedirs: True
    - require:
      - pkg: docker

/etc/systemd/system/docker.service.d/override.conf:
  file.managed:
    - user: root
    - group: root
    - mode: '0644'
    - makedirs: True
    - contents: |
        [Service]
        # keep empty ExecStart= to flush previous entries
        ExecStart=
        ExecStart=/usr/bin/dockerd -H fd:// --experimental \
                  --storage-driver={{ docker.storagedriver | default('overlay2') }}{% if docker.get('ipv6') %} \
                  --ipv6 --fixed-cidr-v6 {{ docker.ipv6.default }}{%- endif %}
    - require:
      - pkg: docker

docker-cleanup:
  cron.present:
    - name: 'docker system prune --force --all{% if 'gitlab-ci' in salt['pillar.get']('roles', []) %} --volumes{% endif %}'
    - hour: '*'
    - minute: '42'
    - require:
      - pkg: docker
