{% set image = 'registry:2' %}

docker-image-{{ image }}:
  docker_image.present:
    - name: {{ image }}
    - force: true
    - watch:
      - pkg: docker
    - require:
      - file: docker-volume-root

docker-image-cache:
  docker_container.running:
    - image: {{ image }}
    - name: image-cache
    - restart_policy: always
    - watch:
      - docker_image: docker-image-{{ image }}
    - require:
      - docker_image: docker-image-{{ image }}
    - environment:
      - REGISTRY_PROXY_REMOTEURL: https://registry-1.docker.io
      - REGISTRY_DELETE_ENABLED: true
      - REGISTRY_HTTP_DEBUG_ADDR: 0.0.0.0:5001
      - REGISTRY_HTTP_DEBUG_PROMETHEUS_ENABLED: true
    - port_bindings:
      - '127.0.0.1:5500:5000'
      - '5501:5001'

docker-image-cache-cleanup:
  cron.present:
    - name: 'docker exec -it image-cache rm -r /var/lib/registry/ || true'
    - hour: '8'
    - minute: '42'
    - require:
      - pkg: docker
