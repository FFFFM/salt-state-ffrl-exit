# -*- coding: utf-8 -*-
import logging
from ipaddress import ip_network

__virtualname__ = 'net_utils'

def __virtual__():
    return __virtualname__

def gateway(ip_range):
    return str(ip_network(ip_range)[1])
