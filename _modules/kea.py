def rfc3397(domains):
    """
    Return a RFC-3397 encoded string (without compression)

    see https://tools.ietf.org/search/rfc3397

    :param string[] domains: A list of domains
    """

    domain_list = ""
    for domain in domains:
        domain_list += ''.join(
            # Split the domain into parts and prepend the length to each
            map(lambda dom: chr(len(dom)) + dom, domain.strip().split('.'))
        ) + "\0"
    return domain_list.encode('utf-8').hex()

def v6_interfaces():
    interfaces = []
    for domain_key, domain in __salt__['pillar.get']('domains', {}).items():
        if 'IPv6' in domain:
            interfaces.append(domain_key + "-bat")

    for subnet in __salt__['pillar.get']('kea_dhcp:v6:subnets', []):
        interfaces.append(subnet.get('interface'))

    if __salt__['pillar.get']('kea_dhcp:v6:interfaces', []):
        interfaces.extend(__salt__['pillar.get']('kea_dhcp:v6:interfaces', []))

    return interfaces
