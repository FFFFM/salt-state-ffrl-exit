def pretty_names():
    domains = {}
    for domain_key, domain in __salt__['pillar.get']('domains').items():
        for domain_code, domain_pretty in domain['domain_names'].items():
            domains[domain_code] = domain_pretty
        if 'domain_gw' in domain:
            domains[domain_key+'-gw'] = domain['domain_gw']
    return domains


def domain_codes():
    codes = []
    for domain in __salt__['pillar.get']('domains').keys():
        codes.extend(__salt__['pillar.get'](
            'domains:%s:domain_names' % domain).keys())
    return codes


def radv_enabled(domain_key):
    host = __salt__['pillar.get']('radv:enabled', True)
    domain = __salt__['pillar.get'](
        'domains:%s:radv:enabled' % domain_key, True)
    return host and domain
