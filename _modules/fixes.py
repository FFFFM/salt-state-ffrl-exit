"""
Various fixes and workarounds
"""


def osmajorrelease():
    """
    Provide a wrapper for osmajorrelease grains as debian bullseye does not set the release number

    :rtype: int
    """
    versions = {
        'Debian-bullseye': 11
    }

    if 'osmajorrelease' in __grains__:
        return __grains__['osmajorrelease']

    return versions['%s-%s' % (__grains__['osfullname'], __grains__['oscodename'])]
