# -*- coding: utf-8 -*-
import logging

"""Expected output as an array for

nextnode.ffffm.net:
    - net: 2001:db8:1::/48
        A: [ 10.176.15.254 ]
        AAAA: [ fd06:8187:fbc0::1:1 ]

so we need:
    - net: domains.dom0.global_prefix6
        A: [domains.dom0.nextnode4]
        AAAA: [domains.dom0.nextnode6]
"""


def knot_yaml():
    # pylint: disable=undefined-variable
    tmp = []
    tmp.append('nextnode.ffffm.net:')
    for domain in __salt__['pillar.get']('domains'):
        global_prefix6 = __salt__['pillar.get']('domains:{}:global_prefix6'.format(domain))
        nextnode4 = __salt__['pillar.get']('domains:{}:nextnode4'.format(domain))
        nextnode6 = __salt__['pillar.get']('domains:{}:nextnode6'.format(domain))
        for prefix6 in global_prefix6:
            tmp.append('  - net: {}'.format(prefix6))
            tmp.append('    A: {}'.format(nextnode4))
            tmp.append('    AAAA: {}'.format(nextnode6))
    return tmp
