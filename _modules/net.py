import logging

# compose mac for interfaces
def clientbr_mac(domain_id, host_id):
    return ":".join(['AA', 'FF', str(domain_id).zfill(2), '00', str(host_id), '04'])

def batadv_mac(domain_id, host_id):
    return ":".join(['AA', 'FF', str(domain_id).zfill(2), '00', str(host_id), '02'])

def fastd_mac(domain_id, host_id):
    return ":".join(['AA', 'FF', str(domain_id).zfill(2), '00', str(host_id), '06'])

def vxlan_mac(domain_id, host_id):
    return ":".join(['AA', 'FF', str(domain_id).zfill(2), '00', str(host_id), '08'])
