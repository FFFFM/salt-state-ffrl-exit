import logging


def all_peerings(proto):
    tmp = []
    selfBGP = __salt__['pillar.get']('peerings:ibgp:self:{}'.format(proto), [])
    for name, peer in sorted(__salt__['pillar.get']('peerings').items()):
        peerproto = __salt__['pillar.get'](
            'peerings:{0}:{1}'.format(name, proto), [])
        if not peerproto:
            continue
        rule = []
        if 'template' in peer:
            template = 'from {} '.format(peer['template'])
        if 'template' in peerproto:
            template = 'from {} '.format(peerproto['template'])
        rule.append('protocol bgp {0} {1}'.format(
            name.replace('-', '_').lower(), template) + '{')

        if 'description' in peer:
            rule.append('\t# description "{}";'.format(peer['description']))
        if 'local_asn' in peer:
            rule.append('\tlocal as {};'.format(peer['local_asn']))
        if 'export_filter' in peerproto:
            rule.append('\texport {};'.format(peerproto['export_filter']))
        if 'import_filter' in peerproto:
            rule.append('\timport {};'.format(peerproto['import_filter']))

        if 'receive_limit' in peerproto:
            rule.append('\treceive limit {} action restart;'.format(peerproto['receive_limit']))

        if 'self' in peerproto:
            rule.append('\tsource address {};'.format(peerproto['self']))
        else:
            rule.append('\tsource address {};'.format(selfBGP))

        rule.append('\tneighbor {0} as {1};'.format(
            peerproto['remote'], peer['asn']))

        if 'preference' in peerproto:
            rule.append('\tpreference {};'.format(peerproto['preference']))

        if 'multihop' in peer:
            rule.append('\tmultihop;')

        if 'nexthop' in peerproto:
            rule.append('\tnext hop {};'.format(peerproto['nexthop']))

        if 'password' in peer:
            rule.append('\tpassword "{}";'.format(peer['password']))

        rule.append('}')
        tmp.extend(rule)
    return tmp



def internal_peerings(proto):
    tmp = []
    if 'bgp6' == proto:
        source_address = __salt__['pillar.get']('canonical_ip_6',"")
    if 'bgp4' == proto:
        source_address = __salt__['pillar.get']('canonical_ip_4',"")
    for name, peer in sorted(__salt__['pillar.get']('bgp').items()):
        peerproto = __salt__['pillar.get'](
            'bgp:{0}:{1}'.format(name, proto), [])
        if not peerproto:
            continue
        rule = []
        if 'template' in peer:
            template = 'from {} '.format(peer['template'])
        rule.append('protocol bgp {0} {1}'.format(
            name.replace('-', '_').lower(), template) + '{')

        rule.append('\tsource address {};'.format(source_address))

        rule.append('\tneighbor {0} as {1};'.format(
            peerproto['remote'], peer['asn']))

        rule.append('}')
        tmp.extend(rule)
    return tmp
