{%- set gopath = salt['pillar.get']('golang:gopath', '/usr/local/go') %}
{%- set project = 'cpthook' %}
{%- set uri = 'github.com/fleaz/CptHook' %}

include:
  - golang

{{ project }}:
  git.latest:
    - name: https://{{ uri }}
    - target: /usr/src/{{ project }}
    - force_fetch: True
    - force_reset: True
    - refspec_branch: main
  cmd.run:
    - name: go install -v {{ uri }}
    - env:
        GOPATH: {{ gopath }}
        GOCACHE: /tmp/GOCACHE/
        GO111MODULE: "on"
    - require:
      - pkg: golang
      - git: {{ project }}
    - onchanges:
      - git: {{ project }}
  service.running:
    - enable: True
    - require:
      - file: /etc/systemd/system/{{ project }}.service
    - watch:
      - file: /etc/systemd/system/{{ project }}.service
      - file: /etc/{{ project }}.yml
      - cmd: {{ project }}

/etc/systemd/system/{{ project }}.service:
  file.managed:
    - source: salt://cpthook-gitlab/files/{{ project }}.service
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - context:
        gopath: {{ gopath }}

/etc/ssl/certs/hackint.crt:
  file.absent

/etc/{{ project }}.yml:
  file.managed:
    - source: salt://cpthook-gitlab/files/{{ project }}.yml
    - user: root
    - group: root
    - mode: '0644'
