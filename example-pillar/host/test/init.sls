roles:
  - test
  - router
  - ffrl-exit
  - babel
  - gateway

pipe_main_export_filter: accept_all

ifaces:
  lo:
    prefixes:
      - 2001:db8::1/128
  eth0:
    lldp: on
    prefixes:
      - 192.0.2.1/29
      - 2001:db8:1::/64
    gateway:
      - 192.0.2.42/24
      - fe80::1
    vrf: vrf_external

  gretap-test:
    mode: gretap
    prefixes:
      - 192.0.2.23/24
      - 2001:db8:babe::1/64
    remote: 192.168.12.1
    local: 192.168.14.5
    ttl: 1
    dev: eth0

  # NAT IP
  nat:
    link-type: dummy
    prefixes:
      - 192.0.2.200/32
