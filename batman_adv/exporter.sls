/usr/local/sbin/prometheus-batadv-txtexport:
  file.managed:
    - source: salt://batman_adv/files/prometheus-batadv-txtexport.sh
    - mode: '0700'

/etc/systemd/system/prometheus-batadv-txtexport.service:
  file.managed:
    - source: salt://batman_adv/files/prometheus-batadv-txtexport.service
    - mode: '0644'

/etc/systemd/system/prometheus-batadv-txtexport.timer:
  file.managed:
    - source:
      - salt://batman_adv/files/prometheus-batadv-txtexport.timer.{{ grains['oscodename'] }}
      - salt://batman_adv/files/prometheus-batadv-txtexport.timer
    - mode: '0644'

prometheus-batadv-txtexport.timer:
  service.running:
    - enable: True
    - require:
      - file: /usr/local/sbin/prometheus-batadv-txtexport
      - file: /etc/systemd/system/prometheus-batadv-txtexport.service
      - file: /etc/systemd/system/prometheus-batadv-txtexport.timer
