include:
  - systemd
  - kernel
  - batman_adv.kmod
  - batman_adv.batctl
  - batman_adv.exporter
  - network.domains-batman-systemd
