---
batctl:
  pkg.installed:
    - fromrepo: {{ grains['oscodename'] }}-backports

/etc/apt/preferences.d/backports-batctl:
  file.managed:
    - contents: |
        Package: batctl
        Pin: release n={{ grains['oscodename'] }}-backports
        Pin-Priority: 900
