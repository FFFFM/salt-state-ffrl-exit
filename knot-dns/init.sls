{% set knot = pillar['knot-dns'] %}

include:
  - apt.repository.knot-dns
  - ferm
  - .exporter

knot:
  pkg.installed:
    - pkgs:
      - knot
      - knot-module-geoip
  service.running:
    - name: knot
    - enable: True
    - reload: True
    - watch:
      - pkg: knot
      - git: /var/lib/knot/zones

/etc/knot/knot.conf:
  file.managed:
    - source: salt://knot-dns/files/knot.conf.j2
    - user: knot
    - group: knot
    - mode: '0644'
    - template: jinja
    - require:
      - pkg: knot
    - watch_in:
      - service: knot

/var/lib/knot/zones:
  git.latest:
    - name: {{ knot['repository']['remote'] }}
    - branch: {{ knot['repository']['branch'] }}
    - target: /var/lib/knot/zones
    - force_fetch: True
    - force_reset: True
    - user: knot
    - require:
      - pkg: git
      - pkg: knot

/var/lib/knot/nextnode.conf:
  file.managed:
    - source: salt://knot-dns/files/nextnode.conf.j2
    - user: knot
    - group: knot
    - mode: '0644'
    - template: jinja
    - require:
      - pkg: knot
    - watch_in:
      - service: knot

/etc/ferm/conf.d/40-knot.conf:
  file.managed:
    - source: salt://knot-dns/files/ferm.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - require:
      - file: /etc/ferm/conf.d
