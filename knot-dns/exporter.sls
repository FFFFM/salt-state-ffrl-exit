include:
  - apt.repository.knot-dns
  - ferm

knot-exporter:
  pkg.installed: []
  service.running:
    - enable: True
    - reload: True
    - require:
      - file: /etc/systemd/system/knot-exporter.service
    - watch:
      - file: /etc/systemd/system/knot-exporter.service
      - pkg: knot-exporter

/etc/systemd/system/knot-exporter.service:
  file.managed:
    - source: salt://knot-dns/files/knot-exporter.service
    - user: root
    - group: root
    - mode: '0644'

/etc/ferm/conf.d/40-knot-exporter.conf:
  file.managed:
    - source: salt://knot-dns/files/ferm-knot-exporter.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - require:
      - file: /etc/ferm/conf.d

prometheus_knot_export:
  grains.present:
    - value: {{ grains.fqdn }}:9433
