{% import 'restic/init.sls' as restic %}

redis:
  pkg.installed:
{%- if  grains['osfinger'] == "Debian-10" %}
    - fromrepo: buster-backports
{%- endif %}
  file.managed:
    - name: /etc/redis/redis.conf
    - source:
      - salt://redis/files/redis.conf.{{ grains.nodename }}.j2
      - salt://redis/files/redis.conf.j2
    - makedirs: True
    - template: jinja
  service.running:
    - name: redis-server
    - enable: True
    - watch:
      - file: redis

{{ restic.path('redis', '/var/lib/redis') }}

include:
  - kernel.overcommit_memory
