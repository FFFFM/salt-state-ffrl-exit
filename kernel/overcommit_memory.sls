{% set sysctld = "/etc/sysctl.d" %}

vm.overcommit_memory:
  sysctl.present:
    - value: 1
    - config: {{ sysctld }}/overcommit_memory.conf
