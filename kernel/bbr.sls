{% set sysctld = "/etc/sysctl.d" %}

net.core.default_qdisc:
  sysctl.present:
    - value: fq_codel
    - config: {{ sysctld }}/codel.conf

net.ipv4.tcp_congestion_control:
  sysctl.present:
    - value: bbr
    - config: {{ sysctld }}/bbr.conf
