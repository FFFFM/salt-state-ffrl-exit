{% if salt['fixes.osmajorrelease']() >= 10 and salt['pillar.get']('sid-kernel', False) %}
include:
  - apt.repository.sid

kernel-apt-pin:
  file.accumulated:
    - name: apt.sid.pinning_exceptions
    - filename: /etc/apt/preferences.d/sid-pinning
    - text: linux-* iproute2*
    - require_in:
      - file: /etc/apt/preferences.d/sid-pinning
{%- endif %}

packages_kernel:
  pkg.latest:
{% if salt['fixes.osmajorrelease']() >= 10 and salt['pillar.get']('sid-kernel', False) %}
    - fromrepo: sid
{%- elif grains['osfinger'] == "Debian-9" %}
    - fromrepo: stretch-backports
{%- elif grains['osfinger'] == "Debian-11" and salt['pillar.get']('backports-kernel', True) %}
    - fromrepo: bullseye-backports
{%- endif %}
    - pkgs:
      - linux-image-amd64
      - linux-headers-amd64
      - linux-perf
      - iproute2
      - iproute2-doc
