{% import 'restic/init.sls' as restic %}

{%- set gopath = '/var/lib/yanic/go' %}

include:
  - golang
  - ferm

yanic:
  user.present:
    - home: /var/lib/yanic
    - shell: /usr/sbin/nologin
  git.latest:
    - name: https://github.com/FreifunkBremen/yanic
    - target: {{ gopath }}/src/github.com/FreifunkBremen/yanic
    - force_fetch: True
    - force_reset: True
    - rev: 4261ad22882b9fa4b340b1bce51e39d916394303
    - refspec_branch: main
    - user: yanic
    - require:
      - user: yanic
  cmd.run:
    - cwd: {{ gopath }}/src/github.com/FreifunkBremen/yanic
    - name: go get -v github.com/FreifunkBremen/yanic
    - runas: yanic
    - env:
        GOPATH: {{ gopath }}
        GOCACHE: /tmp/GOCACHE/
        GO111MODULE: "on"
    - require:
      - pkg: golang
      - git: yanic
      - user: yanic
    - onchanges:
      - git: yanic

/var/log/yanic:
  file.directory:
    - user: yanic
    - group: adm
    - dir_mode: '2750'
    - require:
      - user: yanic

/etc/yanic:
  file.directory:
    - user: root
    - group: yanic
    - dir_mode: '0755'
    - require:
      - user: yanic

/var/lib/yanic/state:
  file.directory:
    - user: yanic
    - group: yanic
    - dir_mode: '0755'
    - require:
      - user: yanic

{{ restic.path('yanic', '/var/lib/yanic/state') }}

/var/lib/yanic/meshviewer:
  file.directory:
    - user: yanic
    - group: yanic
    - require:
      - user: yanic

/etc/systemd/system/yanic.service:
  file.managed:
    - source: salt://yanic/files/yanic.service
    - user: root
    - group: root
    - mode: '0644'

/etc/ferm/conf.d/40-yanic.conf:
  file.managed:
    - source: salt://yanic/files/ferm.conf.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - require:
      - file: /etc/ferm/conf.d

/etc/yanic/config.toml:
  file.managed:
    - source: salt://yanic/files/config.toml.j2
    - user: root
    - group: root
    - mode: '0644'
    - template: jinja
    - require:
      - file: /etc/yanic
      - file: /var/lib/yanic/meshviewer

yanic.service:
  service.running:
    - enable: True
    - require:
      - file: /etc/systemd/system/yanic.service
      - file: /var/log/yanic
      - file: /etc/yanic/config.toml
    - watch:
      - file: /etc/systemd/system/yanic.service
      - file: /etc/yanic/config.toml
      - cmd: yanic
