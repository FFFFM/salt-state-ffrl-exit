/etc/network/interfaces.d/gretap:
  file.managed:
    - source: salt://gre/files/tap.j2
    - mode: '0644'
    - user: root
    - group: root
    - template: jinja
    - makedirs: True

ifreload-on-gretap:
  cmd.run:
    - name: /sbin/ifreload -af
    - onchanges:
      - file: /etc/network/interfaces.d/gretap
    - require:
      - file: /etc/network/interfaces.d/gretap

/etc/ferm/conf.d/20-gretap.conf:
  file.managed:
    - source: salt://gre/files/ferm-tap.conf.j2
    - template: jinja
    - require:
      - file: /etc/ferm/conf.d
