/etc/network/interfaces.d/gre:
  file.managed:
    - source: salt://gre/files/tun.j2
    - mode: '0644'
    - user: root
    - group: root
    - template: jinja
    - makedirs: True

ifreload-on-gretun:
  cmd.run:
    - name: /sbin/ifreload -af
    - onchanges:
      - file: /etc/network/interfaces.d/gre
    - require:
      - file: /etc/network/interfaces.d/gre

/etc/ferm/conf.d/20-gre.conf:
  file.managed:
    - source: salt://gre/files/ferm-tun.conf.j2
    - template: jinja
    - require:
      - file: /etc/ferm/conf.d
