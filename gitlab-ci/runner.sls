{%- from 'gitlab-ci/map.jinja' import gitlab_ci with context %}

include:
  - docker
  - apt.transport.https
  - apt.dependencies

gitlab-runner:
  pkgrepo.managed:
    - humanname: gitlab-runner
    - name: {{ gitlab_ci.apt_repo }}
    - file: /etc/apt/sources.list.d/runner_gitlab-runner.list
    - key_url: https://packages.gitlab.com/gpg.key
  file.managed:
    - name: /etc/apt/preferences.d/pin-gitlab-runner.pref
    - contents: |
        Explanation: Prefer GitLab provided packages over the Debian native ones
        Package: gitlab-runner
        Pin: origin packages.gitlab.com
        Pin-Priority: 1001
  pkg.installed: []
